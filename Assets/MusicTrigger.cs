﻿using UnityEngine;
using System.Collections;

public class MusicTrigger : MonoBehaviour {

	public AudioClip[] songs;

	int triggerTime = 0;

	public void Trigger(){

		GetComponent<AudioLowPassFilter>().enabled = false;
		GetComponent<AudioHighPassFilter>().enabled = false;

		GetComponent<AudioSource>().clip = songs[triggerTime];

		GetComponent<AudioSource>().Play();
		triggerTime ++;
	}
}
