﻿using UnityEngine;
using System.Collections;

public class MainMenuGridMovement : MonoBehaviour {

	
	Renderer rend;
	Material mat;

	float timer = 0f;
	float cycleInterval = 4f;

	void Start () {

		rend = GetComponent<Renderer>();
		mat = rend.material;
	
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		timer %= cycleInterval;
		mat.SetTextureOffset("_MainTex", new Vector2(0f, 1f - (timer/cycleInterval)));
	}
}
