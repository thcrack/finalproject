﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AffixType{
	None, Prefix, Suffix
}

/*

Affix Rarity System

Tier C		1 Rarity
Tier B		2 Rarity
Tier A		3 Rarity
Tier S		4 Rarity

Color		Rarity Name		Minimum Rarity		Affix Combinations
------------------------------------------------------------------

White		Common			0 Total Rarity		No Affix			Tier C
Yellow		Uncommon		2 Total Rarity		Tier B				Tier C + Tier C
Green		Rare			3 Total Rarity		Tier B + Tier C		Tier B + Tier B
Blue		Fabled			Unique Item Set #1	Tier B (Prefix Only)
Magenta		Unreal			4 Total Rarity		Tier A + Tier B		Tier A + Tier C
Purple		Mythical		6 Total Rarity		Tier A + Tier A		Tier S + Tier B		Tier S + Tier A
Orange		Legendary		Unique Item	Set #2	Tier S (Prefix Only)
Cyan		Omniscient		8 Total Rarity		Tier S + Tier S


*/

public enum AffixRarity{
	Empty, TierC, TierB, TierA, TierS
}

public class WeaponModifier : MonoBehaviour {

	public string nameModifier = "";
	public AffixType type = AffixType.None;
	public AffixRarity rarity = AffixRarity.TierC;
	public float baseDamageModifier = 0f;
	public DamageInfo damageModifier;
	public float critChanceModifier = 0f;
	public float critMultiplierModifier = 0f;
	public float pierceModifier = 0f;
	public float clipSizeModifier = 0f;
	public float rateModifier = 0f;
	public float knockbackModifier = 0f;
	public float reloadTimeModifier = 0f;
	public float accuracyModifier = 0f;

	public List<BuffApplication> buffList;

	public string GetAffixName(){
		if(type == AffixType.Prefix){
			return nameModifier + " ";
		}else if(type == AffixType.Suffix){
			return " " + nameModifier;
		}
		return "";
	}
}
