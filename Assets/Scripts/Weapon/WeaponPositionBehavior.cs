﻿using UnityEngine;
using System.Collections;

public class WeaponPositionBehavior : MonoBehaviour {

	public void SwingHit(){
		if(transform.childCount == 0) return;
		WeaponScript childWeapon = transform.GetChild(0).GetComponent<WeaponScript>();
		if(childWeapon.baseWeaponProperty.weaponType == WeaponType.Melee) childWeapon.SwingHit();
	}
}
