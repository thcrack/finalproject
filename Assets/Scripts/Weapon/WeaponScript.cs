using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum WeaponType {
    Melee,
    Pistol,
    SMG,
    Rifle,
    Shotgun,
    ProjectileLauncher,
    ParticleLauncher
}

public enum WeaponReloadType {
    FullClip,
    Bullet
}

public class WeaponScript : MonoBehaviour
{
    [System.SerializableAttribute]
    public class WeaponProperty
    {
        [Header("Core Property")]
        public string weaponName = "";
        public WeaponType weaponType = WeaponType.Rifle;
        public WeaponRarity weaponRarity = WeaponRarity.Common;

        [Header("DPS Property")]
        public float weaponBaseDamage = 30f;
        public DamageInfo weaponDamageMultiplier;
        [HideInInspector] public DamageInfo weaponDamage;
        public float weaponRPM = 60f;

        [Header("Crit Property")]
        public float weaponAdditionalCritChance = 0f;
        public float weaponAdditionalCritMultiplier = 0f;
        [HideInInspector] public float weaponCritChance = 0f;
        [HideInInspector] public float weaponCritMultiplier = 0f;

        [Header("Power Property")]
        public float weaponPiercePower = 5f;
        public float weaponKnockbackPower = 200f;

        [Header("Reload Property")]
        public WeaponReloadType weaponReloadType = WeaponReloadType.FullClip;
        public float weaponReloadTime = 1.5f;
        public int weaponClipSize = 25;
        public int weaponPelletsCount = 1;

        [Header("Buff Application Property")]
        public List<BuffApplication> buffApplicationList;

        [Header("Misc Property")]
        public float weaponAccuracy = 70f;
        [HideInInspector] public float shootOffset = 5f;
        public float weaponRange = 100f;
    }

    [Header("Status")]
    public bool isEquipped = false;
    public EntityStat weaponHolder;

    [Header("Weapon Property")]
    public WeaponModifier modPrefix;
    public WeaponModifier modSuffix;
    GameObject prefixObject, suffixObject;
    WeaponModifier emptyAffix;
    public WeaponProperty baseWeaponProperty;
    [HideInInspector] public WeaponProperty moddedWeaponProperty;

    [Header("FX")]
    public GameObject GunLinePrefab;
    public GameObject projectile;
    public Color weaponFXColor;

    [HideInInspector] public bool isReloading;
 
    public int clipBullets;
    float timer;
    float attackInterval;
    Ray shootRay;
    RaycastHit shootHit;
    int shootableMask;
    LineRenderer line;
    ParticleSystem gunParticles;
    GameObject[] gunLine;
    AudioSource gunAudio, swingEmpty, swingHit;
    Transform gunPoint;
    Animator anim;
    Light gunLight;
    float effectsDisplayTime = 0.02f;

    float reloadStartTime;

    void Awake ()
    {

        emptyAffix = Global.Instance.EmptyAffix;
        line = GetComponent<LineRenderer>();

        switch(baseWeaponProperty.weaponType)
        {

            case WeaponType.Melee:

            swingEmpty = GetComponents<AudioSource>()[0];
            swingHit = GetComponents<AudioSource>()[1];

            break;

            case WeaponType.ProjectileLauncher:

            gunParticles = GetComponentInChildren<ParticleSystem> ();
            gunPoint = transform.GetChild(0);
            gunAudio = GetComponentInChildren<AudioSource> ();
            gunLight = GetComponentInChildren<Light> ();

            break;

            default: // all raycast-based firearms

            gunParticles = GetComponentInChildren<ParticleSystem> ();
            gunPoint = transform.GetChild(0);
            
            LineRenderer[] oldLines = gunPoint.GetComponentsInChildren<LineRenderer>();
            if(oldLines.Length > 0){
                for(int i = 0; i < oldLines.Length; i++){
                    Destroy(oldLines[i].gameObject);
                }
            }

            gunLine = new GameObject[baseWeaponProperty.weaponPelletsCount];
            for(int i = 0; i < gunLine.Length; i++){
                gunLine[i] = (GameObject) Instantiate(GunLinePrefab, gunPoint);
            }
            gunAudio = GetComponentInChildren<AudioSource> ();
            gunLight = GetComponentInChildren<Light> ();

            break;

        }

    }


    void Update ()
    {
        if(isEquipped){

            switch(baseWeaponProperty.weaponType){

                case WeaponType.Melee:

                break;

                case WeaponType.ProjectileLauncher:

                weaponFXColor.a = Mathf.Lerp(weaponFXColor.a, 0, Time.deltaTime * 10f);

                if(timer >= effectsDisplayTime)
                {
                    DisableEffects();
                }

                CheckReload();

                break;


                default:

                if(gunLine[0].GetComponent<LineRenderer>().enabled){
                    weaponFXColor.a = Mathf.Lerp(weaponFXColor.a, 0, Time.deltaTime * 10f);
                    gunLight.intensity = Mathf.Lerp(gunLight.intensity, 0, Time.deltaTime * 5f);
                    for(int i = 0; i < gunLine.Length; i++){
                        gunLine[i].GetComponent<LineRenderer>().SetColors(weaponFXColor, weaponFXColor);
                    }
                }

                if(timer >= effectsDisplayTime)
                {
                    DisableEffects();
                }

                CheckReload();

                break;
            }

            timer += Time.deltaTime;

        }else{
            Vector3 lineDest = transform.position;
            lineDest.y += 4f;
            line.enabled = true;
            line.SetPosition(0, transform.position);
            line.SetPosition(1, lineDest);
        }
    }

    void CheckReload(){
        if(isReloading && Time.time > reloadStartTime + moddedWeaponProperty.weaponReloadTime){
            switch(baseWeaponProperty.weaponReloadType){
                case WeaponReloadType.FullClip:
            
                LoadClip();
                isReloading = false;

                break;

                case WeaponReloadType.Bullet:

                LoadBullet();
                if(clipBullets < moddedWeaponProperty.weaponClipSize){
                    reloadStartTime = Time.time;
                }else{
                    isReloading = false;
                }
                
                break;
            }
        }
    }

    public void Fire(){
        if(timer >= attackInterval && Time.timeScale != 0){
            switch(baseWeaponProperty.weaponType){
                case WeaponType.Melee:

                Swing();

                break;

                default:

                if(clipBullets > 0){
                    switch(baseWeaponProperty.weaponReloadType){
                        case WeaponReloadType.FullClip:

                        if(!isReloading) Shoot();

                        break;

                        case WeaponReloadType.Bullet:

                        if(isReloading){
                            isReloading = false;
                            timer = 0.25f * attackInterval;
                            anim.SetTrigger("ReturnIdle");
                        }else{
                            Shoot();
                        }

                        break;
                    }
                }else{
                    Reload();
                }

                break;
            }
        }
    }


    public void Reload(){
        if(timer < attackInterval && Time.timeScale != 0){

            if(IsInvoking("Reload")) CancelInvoke("Reload");
            Invoke("Reload", attackInterval - timer);

        }else if(clipBullets < moddedWeaponProperty.weaponClipSize && !isReloading){

            if(baseWeaponProperty.weaponReloadType == WeaponReloadType.FullClip){

                anim.speed = 1f / moddedWeaponProperty.weaponReloadTime;

            }else{

                anim.speed = 1f / ( moddedWeaponProperty.weaponReloadTime * ( moddedWeaponProperty.weaponClipSize - clipBullets ) );

            }
            anim.SetTrigger("FirearmReload");
            isReloading = true;
            reloadStartTime = Time.time;
        }
    }

    public void StopReloading(){
        if(IsInvoking("Reload")) CancelInvoke("Reload");
        isReloading = false;
    }

    public void LoadClip(){ 
        clipBullets = moddedWeaponProperty.weaponClipSize;
    }

    public void LoadBullet(){ 
        clipBullets ++;
    }

    public void SetEquipped(bool input, EntityStat holder){
        if(baseWeaponProperty.weaponType != WeaponType.Melee) DisableEffects();
        isEquipped = input;
        if(input){
            anim = transform.parent.GetComponent<Animator>();
            calStatWithMod();
        }else{
            anim = null;
            isReloading = false;
            if(weaponHolder!=null){
                weaponHolder.equippedWeapon = null;
            }
        }

        Rigidbody rigidbody = gameObject.GetComponent<Rigidbody>();
        Collider collider = gameObject.GetComponent<Collider>();

        rigidbody.isKinematic = input;
        collider.enabled = !input;
        line.enabled = !input;

        weaponHolder = holder;

    }

    public void AssignAffix(WeaponModifier pre, WeaponModifier suf){
        modPrefix = pre;
        modSuffix = suf;
        calStatWithMod();
        Debug.Log(moddedWeaponProperty.weaponName);
    }

    public float GetProgress(){
        if(!isReloading){
            if(moddedWeaponProperty.weaponType == WeaponType.Melee) return -1;
            return  (float)clipBullets / moddedWeaponProperty.weaponClipSize;
        }
        return (Time.time - reloadStartTime) / moddedWeaponProperty.weaponReloadTime;
    }


    public void DisableEffects ()
    {
        if(baseWeaponProperty.weaponType != WeaponType.ParticleLauncher && baseWeaponProperty.weaponType != WeaponType.ProjectileLauncher){
            for(int i = 0; i < gunLine.Length; i++){
                gunLine[i].GetComponent<LineRenderer>().enabled = false;
            }
        }

        gunLight.enabled = false;
    }

    public void OnDeath(){
        DisableEffects ();
        isReloading = false;
        SetEquipped(false, null);
        Rigidbody rigidbody = gameObject.GetComponent<Rigidbody>();
        rigidbody.AddForce(Vector3.up * 3f + Vector3.right * Random.Range(-3f, 3f) + Vector3.forward * Random.Range(-3f, 3f), ForceMode.Impulse);
    }

    public void Kill(){
        Destroy(gameObject);
    }

    void Swing(){

        timer = 0f;

        anim.speed = 1f / attackInterval;
        anim.SetTrigger("MeleeSwing");
        
    }

    public void SwingHit(){
        bool isHit = false;
        bool isCrit = Random.Range(0f, 1f) < moddedWeaponProperty.weaponCritChance;
        RaycastHit[] swingHits = Physics.BoxCastAll(transform.parent.position, new Vector3(moddedWeaponProperty.weaponRange * 0.75f, 0.25f, 0.05f), weaponHolder.transform.forward, weaponHolder.transform.rotation, moddedWeaponProperty.weaponRange);
        foreach(RaycastHit hit in swingHits){
            EntityStat entityStat = hit.collider.GetComponent <EntityStat> ();
            if(entityStat != null && hit.collider.gameObject != weaponHolder.transform.gameObject){
                HitInfo dmgInfo = new HitInfo(moddedWeaponProperty.weaponDamage, weaponHolder, entityStat, isCrit, moddedWeaponProperty.weaponCritMultiplier);
                entityStat.Hit(dmgInfo, true);
                Vector3 relation = entityStat.transform.position - transform.position;
                entityStat.Knockback(hit.point, relation.normalized, moddedWeaponProperty.weaponKnockbackPower);
                isHit = true;
            }
        }
        if(isHit){
            swingHit.pitch = Random.Range(-0.25f, 0f) + Time.timeScale;
            swingHit.Play();
        }else{
            swingEmpty.pitch = Time.timeScale;
            swingEmpty.Play();
        }
    }

    void Shoot ()
    {
        if(IsInvoking("Reload")) CancelInvoke("Reload");
        timer = 0f;
        clipBullets --;

        anim.speed = 1f;
        anim.SetTrigger("FirearmShake");

        gunAudio.pitch = Random.Range(-0.05f, 0.05f) + Time.timeScale;
        gunAudio.Play ();

        gunLight.enabled = true;

        gunParticles.Stop ();
        gunParticles.Play ();

        weaponFXColor.a = 1;
        gunLight.intensity = 1f;


        bool isCrit = Random.Range(0f, 1f) < moddedWeaponProperty.weaponCritChance;

        switch(baseWeaponProperty.weaponType){

            case WeaponType.ProjectileLauncher:

            for(int i = 0; i < moddedWeaponProperty.weaponPelletsCount; i++){
                GameObject newProjectile = (GameObject) Instantiate(projectile);
                newProjectile.transform.position = gunPoint.position;
                ProjectileBehavior newProjectileBehavior = newProjectile.GetComponent<ProjectileBehavior>();
                Vector3 dir = Quaternion.Euler(0, Random.Range(-moddedWeaponProperty.shootOffset/2,moddedWeaponProperty.shootOffset/2),0) * gunPoint.forward;
                newProjectile.transform.rotation = Quaternion.LookRotation(dir);
                newProjectileBehavior.sourceWeapon = this;
                newProjectileBehavior.isCrit = isCrit;
                newProjectileBehavior.SetColor(weaponFXColor);
                newProjectileBehavior.SetDirection(dir);
                newProjectileBehavior.Launch();
            }

            break;

            default:

            for(int i = 0; i < gunLine.Length; i++){
                gunLine[i].GetComponent<LineRenderer>().enabled = true;
                gunLine[i].GetComponent<LineRenderer>().SetPosition (0, gunPoint.position);
                shootRay.origin = gunPoint.position;
                shootRay.direction = Quaternion.Euler(0, Random.Range(-moddedWeaponProperty.shootOffset/2,moddedWeaponProperty.shootOffset/2),0) * gunPoint.forward;

                calEachShotPellet(i, isCrit);
            }

            break;
        }
    }

    void calEachShotPellet(int i, bool isCrit){

        Collider tempDisabledEntity = null;
        float pierceCount = moddedWeaponProperty.weaponPiercePower;
        float rangeLeft = moddedWeaponProperty.weaponRange;

        while(pierceCount > 0){
            if(rangeLeft <= 0) return;
            if(Physics.Raycast (shootRay, out shootHit, rangeLeft, Physics.DefaultRaycastLayers, QueryTriggerInteraction.Ignore)){

                EntityStat entityStat = shootHit.collider.GetComponent <EntityStat> ();

                if(entityStat != null){
                    DamageInfo damageOutput = moddedWeaponProperty.weaponDamage;
                    damageOutput.MultiplyDamageValue( pierceCount / moddedWeaponProperty.weaponPiercePower );

                    HitInfo dmgInfo = new HitInfo(damageOutput, weaponHolder, entityStat, isCrit, moddedWeaponProperty.weaponCritMultiplier);
                    entityStat.Hit(dmgInfo, true);
                    entityStat.Knockback(shootHit.point ,shootRay.direction, moddedWeaponProperty.weaponKnockbackPower * ( pierceCount / moddedWeaponProperty.weaponPiercePower ) );
                    shootRay.origin = shootHit.point;
                    pierceCount -= entityStat.entityResistance.PierceResistance;
                    rangeLeft -= shootHit.distance;

                    if(pierceCount <= 0){
                        gunLine[i].GetComponent<LineRenderer>().SetPosition (1, shootHit.point);
                        if(tempDisabledEntity != null) tempDisabledEntity.enabled = true;
                        return;
                    }

                    if(tempDisabledEntity != null) tempDisabledEntity.enabled = true;
                    shootHit.collider.enabled = false;
                    tempDisabledEntity = shootHit.collider;

                    continue;
                }

                gunLine[i].GetComponent<LineRenderer>().SetPosition (1, shootHit.point);
                Rigidbody rgd = shootHit.collider.GetComponent<Rigidbody>();
                if(rgd != null){
                    rgd.AddForceAtPosition(shootRay.direction.normalized * moddedWeaponProperty.weaponKnockbackPower * (pierceCount/moddedWeaponProperty.weaponPiercePower), shootHit.point, ForceMode.Impulse);
                }
                if(tempDisabledEntity != null) tempDisabledEntity.enabled = true;
                return;

            }
            
            gunLine[i].GetComponent<LineRenderer>().SetPosition (1, shootRay.origin + shootRay.direction * rangeLeft);
            if(tempDisabledEntity != null) tempDisabledEntity.enabled = true;
            return;
        }
    }

    void calStatWithMod(){

        // When to calculate modded property?
        // - On SetEquipped(true)
        // - On AssignAffixes

        if(modPrefix == null) modPrefix = emptyAffix;
        if(modSuffix == null) modSuffix = emptyAffix;

        //instantiate mods

        if(prefixObject == null) prefixObject = (GameObject) Instantiate(modPrefix.gameObject, transform);
        if(suffixObject == null) suffixObject = (GameObject) Instantiate(modSuffix.gameObject, transform);
        modPrefix = prefixObject.GetComponent<WeaponModifier>();
        modSuffix = suffixObject.GetComponent<WeaponModifier>();

        //name
        moddedWeaponProperty.weaponName
        = modPrefix.GetAffixName() + baseWeaponProperty.weaponName + modSuffix.GetAffixName();

        //type
        moddedWeaponProperty.weaponType = baseWeaponProperty.weaponType;
        moddedWeaponProperty.weaponRarity = baseWeaponProperty.weaponRarity;

        //damage
        moddedWeaponProperty.weaponBaseDamage
        = baseWeaponProperty.weaponBaseDamage * ( 1 + (modPrefix.baseDamageModifier + modSuffix.baseDamageModifier));

        moddedWeaponProperty.weaponDamageMultiplier = baseWeaponProperty.weaponDamageMultiplier;

        moddedWeaponProperty.weaponDamage
        = DamageInfo.Multiply(moddedWeaponProperty.weaponBaseDamage,
                            moddedWeaponProperty.weaponDamageMultiplier + modPrefix.damageModifier + modSuffix.damageModifier,
                            1f);

        //crit

        moddedWeaponProperty.weaponAdditionalCritChance = baseWeaponProperty.weaponAdditionalCritChance;
        moddedWeaponProperty.weaponAdditionalCritMultiplier = baseWeaponProperty.weaponAdditionalCritMultiplier;
        moddedWeaponProperty.weaponCritChance = Global.Instance.gameMechanics.baseCritChance + moddedWeaponProperty.weaponAdditionalCritChance + modPrefix.critChanceModifier + modSuffix.critChanceModifier;
        moddedWeaponProperty.weaponCritMultiplier = Global.Instance.gameMechanics.baseCritMultiplier + moddedWeaponProperty.weaponAdditionalCritMultiplier + modPrefix.critMultiplierModifier + modSuffix.critMultiplierModifier;

        //pierce
        moddedWeaponProperty.weaponPiercePower
        = baseWeaponProperty.weaponPiercePower * ( 1 + (modPrefix.pierceModifier + modSuffix.pierceModifier));

        //rate of fire
        moddedWeaponProperty.weaponRPM
        = baseWeaponProperty.weaponRPM * ( 1 + (modPrefix.rateModifier + modSuffix.rateModifier));

        //clip size
        moddedWeaponProperty.weaponClipSize
        = Mathf.CeilToInt((1 + (modPrefix.clipSizeModifier + modSuffix.clipSizeModifier)) * baseWeaponProperty.weaponClipSize);
        
        //knockback
        moddedWeaponProperty.weaponKnockbackPower
        = baseWeaponProperty.weaponKnockbackPower * (1 + (modPrefix.knockbackModifier + modSuffix.knockbackModifier));

        //reload time
        moddedWeaponProperty.weaponReloadTime
        = baseWeaponProperty.weaponReloadTime * ( 1 + (modPrefix.reloadTimeModifier + modSuffix.reloadTimeModifier));

        // accuracy

        moddedWeaponProperty.weaponAccuracy = baseWeaponProperty.weaponAccuracy;
        moddedWeaponProperty.shootOffset = Mathf.Pow((1f - (Mathf.Min(100f,moddedWeaponProperty.weaponAccuracy)/100f))*10f, 2f);
        // accuracyModifier = 0f;

        moddedWeaponProperty.buffApplicationList
        = new List<BuffApplication>(baseWeaponProperty.buffApplicationList);

        moddedWeaponProperty.weaponPelletsCount = baseWeaponProperty.weaponPelletsCount;
        moddedWeaponProperty.weaponRange = baseWeaponProperty.weaponRange;

        // get buff from mods

        foreach(BuffApplication buffApp in modPrefix.buffList){
            moddedWeaponProperty.buffApplicationList.Add(buffApp);
        }

        foreach(BuffApplication buffApp in modSuffix.buffList){
            moddedWeaponProperty.buffApplicationList.Add(buffApp);
        }

        //buff dmg update

        foreach(BuffApplication buffApp in moddedWeaponProperty.buffApplicationList){
            if(buffApp.overrideProperty.inheritBaseDamage) buffApp.overrideProperty.baseDamage = moddedWeaponProperty.weaponBaseDamage;
        }

        attackInterval = 60 / moddedWeaponProperty.weaponRPM;
        timer = attackInterval;

        //update color

        Color rarityColor = GetRarityColor();
        Color fadeColor = rarityColor;
        fadeColor.a = 0f;

        line.SetColors(rarityColor, fadeColor);
        line.SetWidth(0.05f, 0.05f);

        if(baseWeaponProperty.weaponRarity != WeaponRarity.Fabled && baseWeaponProperty.weaponRarity != WeaponRarity.Legendary){

            DamageType dmgType = moddedWeaponProperty.weaponDamage.GetLargestDamageType(true);

            if(dmgType != DamageType.Normal){

                Color newColor = DamageInfo.GetDamageColor(dmgType);

                Renderer[] rends = GetComponentsInChildren<Renderer>();

                foreach(Renderer rend in rends){

                    Material mat = rend.material;
                    mat.SetColor("_EmissionColor", newColor);

                }

                switch(baseWeaponProperty.weaponType){

                    case WeaponType.Melee:
                    break;

                    case WeaponType.ProjectileLauncher:

                    weaponFXColor = newColor;
                    gunLight.color = newColor;
                    break;
                    
                    default:
                    weaponFXColor = newColor;
                    gunLight.color = newColor;
                    for(int i = 0; i < gunLine.Length; i++){
                        gunLine[i].GetComponent<LineRenderer>().SetColors(newColor, newColor);
                        gunParticles.startColor = newColor;
                    }
                    break;
                }
            }
        }

    }

    public Color32 GetRarityColor(){
        Color32 result = Color.white;
        switch(baseWeaponProperty.weaponRarity){
            case WeaponRarity.Common:
            result = Color.white;
            break;

            case WeaponRarity.Uncommon:
            result = new Color32(255,255,0,255);
            break;

            case WeaponRarity.Rare:
            result = new Color32(0,255,0,255);
            break;

            case WeaponRarity.Fabled:
            result = new Color32(0,102,255,255);
            break;

            case WeaponRarity.Unreal:
            result = new Color32(255,0,255,255);
            break;

            case WeaponRarity.Mythical:
            result = new Color32(204,51,255,255);
            break;

            case WeaponRarity.Legendary:
            result = new Color32(255,102,0,255);
            break;

            case WeaponRarity.Omniscient:
            result = new Color32(0,255,255,255);
            break;
        }
        return result;
    }

    public string GetRarityColorName(){
        string colorName = "";
        switch(baseWeaponProperty.weaponRarity){
            case WeaponRarity.Common:
            colorName = "white";
            break;

            case WeaponRarity.Uncommon:
            colorName = "yellow";
            break;

            case WeaponRarity.Rare:
            colorName = "lime";
            break;

            case WeaponRarity.Fabled:
            colorName = "#0066ff";
            break;

            case WeaponRarity.Unreal:
            colorName = "magenta";
            break;

            case WeaponRarity.Mythical:
            colorName = "#cc33ff";
            break;

            case WeaponRarity.Legendary:
            colorName = "#ff6600";
            break;

            case WeaponRarity.Omniscient:
            colorName = "cyan";
            break;
        }
        return colorName;
    }

    public string GetTypeString(){
        string result = "";
        switch(moddedWeaponProperty.weaponType){
            case WeaponType.ProjectileLauncher:
            result = "Projectile Launcher";
            break;

            case WeaponType.ParticleLauncher:
            result = "Particle Launcher";
            break;

            case WeaponType.Melee:
            result = "Melee Weapon";
            break;

            default:
            result = moddedWeaponProperty.weaponType.ToString();
            break;
        }

        return result;
    }

    public string GetStylizedName(){
        string result = "<color=" + GetRarityColorName() + ">" + moddedWeaponProperty.weaponName + "</color>";
        return result;
    }
}
