﻿using UnityEngine;
using System.Collections;

public class DisguiseBehavior : MonoBehaviour {

	public bool ChangeMaterial = true;
	public Material targetMaterial;
	public bool ChangeLight = false;
	public Color targetLightColor;

	public void DisableDisguise(){

		if(ChangeMaterial && targetMaterial != null){
			Renderer rend = GetComponent<Renderer>();
			rend.sharedMaterial = targetMaterial;
		}

		Light[] lights = GetComponentsInChildren<Light>();

		if(ChangeLight && lights != null){
			for(int i = 0; i < lights.Length; i++){
				lights[i].color = targetLightColor;
			}
		}

	}
}
