﻿using UnityEngine;
using System.Collections;

public class Map0DinerBehavior : MonoBehaviour {

	public void Trigger(){
		Light[] lights = GetComponentsInChildren<Light>();

		for(int i = 0; i < lights.Length; i++){
			lights[i].enabled = false;
		}

		Invoke("ReactivateLight", 9f);
	}

	void ReactivateLight(){

		BroadcastMessage("DisableDisguise");
		Light[] lights = GetComponentsInChildren<Light>();

		for(int i = 0; i < lights.Length; i++){
			lights[i].enabled = true;
		}
	}
}
