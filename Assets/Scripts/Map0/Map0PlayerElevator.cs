﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Map0PlayerElevator : MonoBehaviour {

	public Transform ElevatorParent;
	public GameObject BackgroundGrid;

	public void Trigger(){
		PlayerManager pm = GetComponent<PlayerManager>();
		PlayerBehavior[] playerList = pm.Players;

		for(int i = 0; i < playerList.Length; i++){
			if(!playerList[i].GetComponent<PlayerBehavior>().entityAttribute.isDead){
				playerList[i].transform.SetParent(ElevatorParent, true);
				playerList[i].rigidbody.isKinematic = true;
				playerList[i].nav.enabled = false;
			}
		}

		Camera.main.GetComponent<CameraFollow>().fixedMode = false;
	}

	public void UndoParent(){
		PlayerManager pm = GetComponent<PlayerManager>();
		PlayerBehavior[] playerList = pm.Players;

		for(int i = 0; i < playerList.Length; i++){
			if(!playerList[i].GetComponent<PlayerBehavior>().entityAttribute.isDead){
				playerList[i].transform.SetParent(transform, true);
				playerList[i].rigidbody.isKinematic = false;
				playerList[i].nav.enabled = true;
			}
		}

		Camera.main.GetComponent<CameraFollow>().fixedMode = true;

		Invoke("EnableGrid", 1f);
	}

	void EnableGrid(){
		BackgroundGrid.SetActive(true);
	}
}
