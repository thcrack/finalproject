﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Map0EightTriggerManager : MonoBehaviour {

	public List<Map0EightRoomTrigger> triggers;
	public List<PodSpawner> podSpawners;
	public GameObject targetDoor;
	public GameObject targetDoorIndicator;
	public GameObject fullClearDoor;
	public GameObject fullClearDoorIndicator;
	public int minLootDrop = 4;
	public int maxLootDrop = 8;
	public RarityWeight weightMod;
	int[] reactionType = new int[8];
	int clearCount = 0;

	// Use this for initialization
	void Start () {

		List<Map0EightRoomTrigger> copy = new List<Map0EightRoomTrigger>(triggers);

		// 4 release enemy pods

		for(int i = 0; i < 4; i++){
			int rngResult = Random.Range(0, copy.Count);
			reactionType[triggers.IndexOf(copy[rngResult])] = 1;
			copy.Remove(copy[rngResult]);
		}

		// 3 drop items

		for(int i = 0; i < 3; i++){
			int rngResult = Random.Range(0, copy.Count);
			reactionType[triggers.IndexOf(copy[rngResult])] = 2;
			copy.Remove(copy[rngResult]);
		}

		// 1 opens the door

		reactionType[triggers.IndexOf(copy[0])] = 3;

	}
	
	public void Trigger(Map0EightRoomTrigger input){

		int roomNumber = triggers.IndexOf(input);
		switch(reactionType[roomNumber]){

			case 1: //enemy
			int rng = Random.Range(0, podSpawners.Count);
			podSpawners[rng].Trigger();
			podSpawners.RemoveAt(rng);
			rng = Random.Range(0, podSpawners.Count);
			podSpawners[rng].Trigger();
			podSpawners.RemoveAt(rng);
			break;

			case 2: //loot
			Global.Instance.WeaponSpawnManager.SpawnMultipleWeapon(input.transform.position, input.transform.forward, Random.Range(minLootDrop, maxLootDrop), weightMod);
			break;

			case 3: //door
			targetDoor.SendMessage("Trigger");
			targetDoorIndicator.SendMessage("Trigger");
			break;

		}
		clearCount++;
		if(clearCount == 8){
			fullClearDoor.SendMessage("Trigger");
			fullClearDoorIndicator.SendMessage("Trigger");
		}

	}
}
