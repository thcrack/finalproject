﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Map0DisguiseUncover : MonoBehaviour {

	public GameObject EnemyPrefab;
	List<EnemyBehavior> enemyList;

	int civCount;

	void Awake(){

		enemyList = new List<EnemyBehavior>();

		civCount = transform.childCount;

		for(int i = 0; i < civCount; i++){
			GameObject newEnemy = (GameObject) Instantiate(EnemyPrefab, transform.GetChild(i).position, transform.GetChild(i).rotation);
			enemyList.Add(newEnemy.GetComponent<EnemyBehavior>());
			newEnemy.transform.SetParent(transform);
			newEnemy.SetActive(false);
		}

	}

	public void Trigger(){

		for(int i = 0; i < civCount; i++){
			enemyList[i].gameObject.SetActive(true);
			transform.GetChild(i).gameObject.SetActive(false);
		}

	}

	public void StartChasing(){

		for(int i = 0; i < civCount; i++){

			enemyList[i].FindRandomPlayer();
			
		}

	}
}
