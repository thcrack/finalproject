﻿using UnityEngine;
using System.Collections;

public class Map0ElevatorBehavior : MonoBehaviour{

	Animator anim;
	bool isOnGround;

	public GameObject Pod;

	public void Trigger(){
		if(isOnGround){
			Debug.Log("MoveDown");
			anim.SetTrigger("MoveDown");
		}else{
			Debug.Log("MoveUp");
			anim.SetTrigger("MoveUp");
		}
	}

	void Awake(){
		anim = GetComponent<Animator>();
	}

	void ArriveAtGround(){
		isOnGround = true;
		Invoke("SendTriggerPod", 1f);
	}

	void SendTriggerPod(){
		Pod.SendMessage("Trigger");
	}

	void UnbindPlayer(){
		Global.Instance.PlayerManager.GetComponent<Map0PlayerElevator>().UndoParent();
		Camera.main.SendMessage("Trigger");
	}
}
