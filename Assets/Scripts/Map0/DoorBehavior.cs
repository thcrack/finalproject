﻿using UnityEngine;
using System.Collections;

public class DoorBehavior : MonoBehaviour {

	public bool isOpened = false;
	public float doorHeight = 2.8f;
	public float moveSmooth = 10f;
	public float stoppingOffset = 0.001f;
	float initialY;
	float targetY;

	// Use this for initialization
	void Start () {

		initialY = transform.position.y;
		targetY = initialY - doorHeight;
	
	}
	
	// Update is called once per frame
	void Update () {

		if(isOpened && transform.position.y > targetY + stoppingOffset){
			float newY = Mathf.Lerp(transform.position.y, targetY, moveSmooth * Time.deltaTime);
			transform.position = new Vector3(transform.position.x, newY, transform.position.z);
		}else if(!isOpened && transform.position.y < initialY - stoppingOffset){
			float newY = Mathf.Lerp(transform.position.y, initialY, moveSmooth * Time.deltaTime);
			transform.position = new Vector3(transform.position.x, newY, transform.position.z);
		}
	
	}

	public void Trigger(){
		isOpened = !isOpened;
	}
}
