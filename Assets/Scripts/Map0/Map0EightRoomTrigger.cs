﻿using UnityEngine;
using System.Collections;

public class Map0EightRoomTrigger : MonoBehaviour {

	public Map0EightTriggerManager triggerManager;

	public void Trigger(){
		triggerManager.Trigger(this);
	}
}
