﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuButtonActions : MonoBehaviour {

	public void Trigger(string input){

		if(input == "StartGame"){
			SceneManager.LoadSceneAsync("Main", LoadSceneMode.Single);
		}else{
			Application.Quit();
		}

	}
}
