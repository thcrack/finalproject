using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HitInfo{

	// 6 Elements in []Damage
	// {Normal, RAD, Cold, Heat, Acid, Volt}

	public static int DamageTypeCount = 6;

	public DamageInfo Damage;
	public bool isCrit;
	public float critMultiplier = 1f;
	public EntityStat Damager;
	public EntityStat Receiver;

	public float GetDamage(){

		float[] resistanceData = Receiver.GetResistanceArray();
		float damageSum = 0f;
		float largestDamage = 0f;
		DamageType largestDamageType = DamageType.Normal;
		float[] damageArray = Damage.GetArray();

		for(int i = 0; i < damageArray.Length; i++){

			//Debug.Log((DamageType)i + " Damage: " + damageArray[i]);

			float singleDamage = damageArray[i] * ( 1f - resistanceData[i] );

			if(singleDamage > 0f ){

				if(isCrit) singleDamage *= critMultiplier;

				if(Damager.entityAttribute.faction == Receiver.entityAttribute.faction){
					singleDamage *= Global.Instance.gameMechanics.TKDamagePercentage;
				}

				damageSum += singleDamage;

				if(Global.Instance.gameSettings.damageNumberMode == DamageNumberMode.Single && singleDamage > largestDamage && i != 0){
					largestDamageType = (DamageType) i;
					largestDamage = singleDamage;
				}else if(Global.Instance.gameSettings.damageNumberMode == DamageNumberMode.Multiple){
					Global.Instance.HUDManager.CreateDamagePopup(Receiver.transform, singleDamage, DamageInfo.GetDamageColor((DamageType)i), isCrit);
				}

			}
			
		}

		//Debug.Log(largestDamageType);

		if(Global.Instance.gameSettings.damageNumberMode == DamageNumberMode.Single){
			Global.Instance.HUDManager.CreateDamagePopup(Receiver.transform, damageSum, DamageInfo.GetDamageColor(largestDamageType), isCrit);
		}

		//Debug.Log(Damager + "/" + Receiver + "/" + damageSum);

		return damageSum;

	}

	public List<BuffApplication> GetBuffOnHit(){
		if(Damager.entityAttribute.faction == Receiver.entityAttribute.faction && Receiver.entityAttribute.faction == EntityFaction.Player) return null;
		List<BuffApplication> resultList = new List<BuffApplication>();
		List<BuffApplication> buffList = Damager.equippedWeapon.moddedWeaponProperty.buffApplicationList;
		foreach(BuffApplication buff in buffList){
			if(buff.applyingCondition == BuffApplyingCondition.OnHit && Random.Range(0f, 1f) <= buff.applyingChance){
				resultList.Add(buff);
			}
		}
		return resultList;
	}

	public HitInfo(DamageInfo Damage, EntityStat Damager, EntityStat Receiver){
		this.Damage = Damage;
		this.Damager = Damager;
		this.Receiver = Receiver;
	}

	public HitInfo(DamageInfo Damage, EntityStat Damager, EntityStat Receiver, bool isCrit, float critMultiplier){
		this.isCrit = isCrit;
		this.critMultiplier = critMultiplier;
		this.Damage = Damage;
		this.Damager = Damager;
		this.Receiver = Receiver;
	}

	public HitInfo(float simpleDamage, EntityStat Damager, EntityStat Receiver){
		Damage.NormalDamage = simpleDamage;
		this.Damager = Damager;
		this.Receiver = Receiver;
	}

	public HitInfo(float BaseDamage, float[] DamageRatio, EntityStat Damager, EntityStat Receiver){

		if(DamageRatio.Length != DamageTypeCount){
			Debug.LogError("Damage Ratio Input does not match with damage type.");
			return;
		}

		for(int i = 0; i < DamageTypeCount; i++){
			Damage.SetDamageValue(i, BaseDamage * DamageRatio[i]);
		}

		this.Damager = Damager;
		this.Receiver = Receiver;
	}

}
