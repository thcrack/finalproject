﻿using UnityEngine;
using System.Collections;

public class PlayerAIStateChase : IPlayerAIState {

	PlayerBehavior player;
	bool isAimed = false;

	public PlayerAIStateChase(PlayerBehavior player){
		this.player = player;
	}

	public void UpdateState()
	{

		if(player.enemyTarget.entityAttribute.isDead){
			player.enemyTarget = null;
			player.movingTarget = player.transform.position;
			ToMoveState();
		}

		isAimed = player.Aim();

		if(isAimed)
		{
			player.FireWeapon();
		}

	}

	public void FixedUpdateState(){

		if(!isAimed){
			player.nav.destination = player.enemyTarget.transform.position;
		}else{
			player.nav.destination = player.transform.position;
		}

	}

	public void ToFollowState()
	{
		player.enemyTarget = null;
		player.playerAIState.currentState = player.playerAIState.followState;
	}

	public void ToMoveState()
	{
		player.enemyTarget = null;
		player.playerAIState.currentState = player.playerAIState.moveState;
	}

	public void ToInteractState()
	{
		player.enemyTarget = null;
		player.playerAIState.currentState = player.playerAIState.interactState;
	}

	public void ToChaseState()
	{
		// Can't change to itself
	}
}
