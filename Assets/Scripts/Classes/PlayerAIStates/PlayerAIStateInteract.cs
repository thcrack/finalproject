﻿using UnityEngine;
using System.Collections;

public class PlayerAIStateInteract : IPlayerAIState {

	PlayerBehavior player;

	public PlayerAIStateInteract(PlayerBehavior player){
		this.player = player;
	}

	public void UpdateState()
	{
		if(player.Aim())
		{
			player.FireWeapon();
		}

	}

	public void FixedUpdateState(){
		
		player.nav.destination = Global.Instance.PlayerManager.MainPlayer.transform.position;

	}

	public void ToFollowState()
	{
		player.playerAIState.currentState = player.playerAIState.followState;
	}

	public void ToMoveState()
	{
		player.playerAIState.currentState = player.playerAIState.moveState;
	}

	public void ToInteractState()
	{
		// Can't change to itself
	}

	public void ToChaseState()
	{
		player.playerAIState.currentState = player.playerAIState.chaseState;
	}
}
