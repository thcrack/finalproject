﻿using UnityEngine;
using System.Collections;

public class PlayerAIStateMove : IPlayerAIState {

	PlayerBehavior player;

	public PlayerAIStateMove(PlayerBehavior player){
		this.player = player;
	}

	public void UpdateState()
	{
		if(player.Aim())
		{
			player.FireWeapon();
		}

	}

	public void FixedUpdateState(){
		
		player.nav.destination = player.movingTarget;

	}

	public void ToFollowState()
	{
		player.playerAIState.currentState = player.playerAIState.followState;
	}

	public void ToMoveState()
	{
		// Can't change to itself
	}

	public void ToInteractState()
	{
		player.playerAIState.currentState = player.playerAIState.interactState;
	}

	public void ToChaseState()
	{
		player.playerAIState.currentState = player.playerAIState.chaseState;
	}
}
