﻿using UnityEngine;
using System.Collections;

public class PlayerAIStateFollow : IPlayerAIState {

	PlayerBehavior player;

	public PlayerAIStateFollow(PlayerBehavior player){
		this.player = player;
	}

	public void UpdateState()
	{
		if(player.Aim())
		{
			player.FireWeapon();
		}

	}

	public void FixedUpdateState(){
		
		player.nav.destination = Global.Instance.PlayerManager.MainPlayer.transform.position;

	}

	public void ToFollowState()
	{
		// Can't change to itself
	}

	public void ToMoveState()
	{
		player.playerAIState.currentState = player.playerAIState.moveState;
	}

	public void ToInteractState()
	{
		player.playerAIState.currentState = player.playerAIState.interactState;
	}

	public void ToChaseState()
	{
		player.playerAIState.currentState = player.playerAIState.chaseState;
	}
}
