﻿using UnityEngine;
using System.Collections;

public enum BuffApplyingCondition{
	OnHit,
	OnHurt,
	OnCrit
}

[System.SerializableAttribute]
public class BuffApplication {

	[Header("Buff Prefab")]
	public BuffBehavior buffPrefab;

	[Header("Application Behavior")]
	public BuffApplyingCondition applyingCondition;
	[Range(0f,1f)]
	public float applyingChance = 0f;

	[Header("Buff Parameter Override")]
	public bool enableOverride = true;
	public BuffProperty overrideProperty;

	public string GetDescription(){

		string result = "";

		if(applyingChance < 1f){
			result += Mathf.FloorToInt(applyingChance*100) + "% chance ";
		}

		// condition
		switch(applyingCondition){
			case BuffApplyingCondition.OnHit:
			result += "<color=orange>On Hit:</color> \n";
			break;
			case BuffApplyingCondition.OnHurt:
			result += "<color=orange>On Hurt:</color> \n";
			break;
			case BuffApplyingCondition.OnCrit:
			result += "<color=orange>On Crit:</color> \n";
			break;
		}

		// effects

		if(enableOverride){
			if(overrideProperty.movementMod){
				result += "\tslows target for " + Mathf.FloorToInt((1f-overrideProperty.movementModAmount)*100) + "%";
				if(overrideProperty.damageOverTime) result += "\n";
			}
			if(overrideProperty.damageOverTime){
				DamageType largestType = overrideProperty.damageOverTimeDPS.GetLargestDamageType(true);
				result += "\tdeals " + (overrideProperty.damageOverTimeDPS * overrideProperty.baseDamage).Sum()
						+ " <color=#" + Global.ColorToHex(DamageInfo.GetDamageColor(largestType)) + ">" + largestType.ToString() + "</color> damage/s";
			}
			if(!overrideProperty.isPermanent){
				result += " for " + overrideProperty.duration + "s";
			}
		}

		return result;


	}
	
}

[System.SerializableAttribute]
public class BuffProperty {

	[Header("Duration")]
	public bool isPermanent;
	public float duration;
	[Header("Movement")]
	public bool movementMod;
	public float movementModAmount;
	[Header("Damage Over Time")]
	public bool damageOverTime;
	public bool inheritBaseDamage;
	public float baseDamage;
	public DamageInfo damageOverTimeDPS;

}

public enum BuffType{
	Buff,
	Neutral,
	Debuff
}

[System.SerializableAttribute]
public class BuffBehavior : MonoBehaviour{

	public string name;
	public bool isActive = false;
	public WeaponScript source;
	[HideInInspector] public EntityStat buffApplier;
	public float timer = 0f;
	public BuffType type = BuffType.Buff;
	public BuffProperty property;
	HitInfo hitInfoPool;
	float dotLastTickTime;

	public void ApplyEffect(EntityStat entity){
		if(property.movementMod) entity.entityBuffMod.movementMod = Mathf.Min(entity.entityBuffMod.movementMod, property.movementModAmount);
		if(property.damageOverTime && (dotLastTickTime - timer > Global.Instance.gameMechanics.DOTTickInterval))
		{
			dotLastTickTime -= Global.Instance.gameMechanics.DOTTickInterval;
			//float baseDamage = (property.inheritBaseDamage) ? property.inheritedBaseDamage : property.baseDamage;
			DamageInfo deltaDmg = DamageInfo.Multiply(property.baseDamage, property.damageOverTimeDPS, 0f);
			deltaDmg.MultiplyDamageValue(Global.Instance.gameMechanics.DOTTickInterval);
			if(hitInfoPool == null){
				hitInfoPool = new HitInfo(deltaDmg, buffApplier, entity);
			}else{
				hitInfoPool.Damage = deltaDmg;
			}
			entity.Hit(hitInfoPool, false);
		}

		timer -= Time.deltaTime;

		if(!property.isPermanent && timer <= 0f)
		{
			isActive = false;
			Destroy(gameObject);
		}
	}

	public void ActivateBuff(BuffApplication buffApp, EntityStat applier){

		buffApplier = applier;

        if(buffApp.enableOverride){

            property = buffApp.overrideProperty;
                
        }

		isActive = true;
		timer = property.duration;
		dotLastTickTime = timer;
	}

}
