using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum DamageType {
	Normal = 0,
	RAD = 1,
	Cold = 2,
	Heat = 3,
	Acid = 4,
	Volt = 5
}

[System.SerializableAttribute]
public struct DamageInfo{
	[Tooltip("Normal damage weight has a base of 1 when multiplying base damage.")]
	public float NormalDamage;
    [Tooltip("RAD Resistance and armor are always ignored.")]
	public float RADDamage;
	public float ColdDamage;
	public float HeatDamage;
	public float AcidDamage;
	public float VoltDamage;

	public DamageInfo(float a, float b, float c, float d, float e, float f){
		NormalDamage = a;
		RADDamage = b;
		ColdDamage = c;
		HeatDamage = d;
		AcidDamage = e;
		VoltDamage = f;
	}

	public float[] GetArray(){
		return new float[]{NormalDamage, RADDamage, ColdDamage, HeatDamage, AcidDamage, VoltDamage};
	}

	public float Sum(){
		return NormalDamage + RADDamage + ColdDamage + HeatDamage + AcidDamage + VoltDamage;
	}

	public DamageType GetLargestDamageType(bool ignoreNormal){
		float[] dmgArray = GetArray();
		float largestDamage = 0f;
		int largestIndex = 0;
		for(int i = 0; i < dmgArray.Length; i++){
			if(dmgArray[i] > largestDamage && !(ignoreNormal && i == 0)){
				largestDamage = dmgArray[i];
				largestIndex = i;
			}
		}
		return (DamageType)largestIndex;
	}

	public void MultiplyDamageValue(float value){
		NormalDamage *= value;
		RADDamage *= value;
		ColdDamage *= value;
		HeatDamage *= value;
		AcidDamage *= value;
		VoltDamage *= value;
	}

	public void MultiplyByRatio(DamageInfo ratio){
		NormalDamage *= ratio.NormalDamage;
		RADDamage *= ratio.RADDamage;
		ColdDamage *= ratio.ColdDamage;
		HeatDamage *= ratio.HeatDamage;
		AcidDamage *= ratio.AcidDamage;
		VoltDamage *= ratio.VoltDamage;
	}

	public void MultiplyByRatio(DamageInfo ratio, float baseRatio){
		NormalDamage *= baseRatio + ratio.NormalDamage;
		RADDamage *= baseRatio + ratio.RADDamage;
		ColdDamage *= baseRatio + ratio.ColdDamage;
		HeatDamage *= baseRatio + ratio.HeatDamage;
		AcidDamage *= baseRatio + ratio.AcidDamage;
		VoltDamage *= baseRatio + ratio.VoltDamage;
	}

	public static DamageInfo operator +(DamageInfo a, DamageInfo b){

		DamageInfo result;

		result.NormalDamage = a.NormalDamage + b.NormalDamage;
		result.RADDamage = a.RADDamage + b.RADDamage;
		result.ColdDamage = a.ColdDamage + b.ColdDamage;
		result.HeatDamage = a.HeatDamage + b.HeatDamage;
		result.AcidDamage = a.AcidDamage + b.AcidDamage;
		result.VoltDamage = a.VoltDamage + b.VoltDamage;

		return result;
	}

	public static DamageInfo Combine(DamageInfo a, DamageInfo b, DamageInfo c){

		DamageInfo result;

		result.NormalDamage = a.NormalDamage + b.NormalDamage + c.NormalDamage;
		result.RADDamage = a.RADDamage + b.RADDamage + c.RADDamage;
		result.ColdDamage = a.ColdDamage + b.ColdDamage + c.ColdDamage;
		result.HeatDamage = a.HeatDamage + b.HeatDamage + c.HeatDamage;
		result.AcidDamage = a.AcidDamage + b.AcidDamage + c.AcidDamage;
		result.VoltDamage = a.VoltDamage + b.VoltDamage + c.VoltDamage;

		return result;
	}

	public static DamageInfo operator *(DamageInfo dmgInfo, float amount){

		DamageInfo result;

		result.NormalDamage = amount * dmgInfo.NormalDamage;
		result.RADDamage = amount * dmgInfo.RADDamage;
		result.ColdDamage = amount * dmgInfo.ColdDamage;
		result.HeatDamage = amount * dmgInfo.HeatDamage;
		result.AcidDamage = amount * dmgInfo.AcidDamage;
		result.VoltDamage = amount * dmgInfo.VoltDamage;

		return result;

	}

	public static DamageInfo Multiply(float baseDamage, DamageInfo ratio, float normalBaseRatio){

		DamageInfo result;

		result.NormalDamage = baseDamage * ( normalBaseRatio + ratio.NormalDamage);
		result.RADDamage = baseDamage * ( ratio.RADDamage);
		result.ColdDamage = baseDamage * ( ratio.ColdDamage);
		result.HeatDamage = baseDamage * ( ratio.HeatDamage);
		result.AcidDamage = baseDamage * ( ratio.AcidDamage);
		result.VoltDamage = baseDamage * ( ratio.VoltDamage);

		return result;

	}

	public void SetDamageValue(int i, float value){
		switch(i){
			case 0: NormalDamage = value; break;
			case 1: RADDamage = value; break;
			case 2: ColdDamage = value;	break;
			case 3:	HeatDamage = value;	break;
			case 4:	AcidDamage = value;	break;
			case 5:	VoltDamage = value;	break;
		}
	}

	public static Color GetDamageColor(DamageType dmgType){
		Color result = new Color32(0, 0, 0, 0);
		switch(dmgType){
			case DamageType.Normal: 	result =  new Color32(255, 255, 255, 255); break;
			case DamageType.RAD: 		result =  new Color32(255, 0, 255, 255); break;
			case DamageType.Cold: 		result =  new Color32(0, 160, 255, 255); break;
			case DamageType.Heat: 		result =  new Color32(255, 50, 0, 255); break;
			case DamageType.Acid: 		result =  new Color32(60, 255, 0, 255); break;
			case DamageType.Volt: 		result =  new Color32(255, 210, 0, 255); break;
		}
		return result;
	}
}