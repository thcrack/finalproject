﻿using UnityEngine;
using System.Collections;

public class CameraMouseOverInteraction : MonoBehaviour {

	int itemMask;

	// Use this for initialization
	void Start () {
		itemMask = LayerMask.GetMask("Item");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(Camera.main == null) return;
		Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit itemHit;
		if (Physics.SphereCast (camRay, 0.25f, out itemHit, 100f, itemMask, QueryTriggerInteraction.Ignore)
			&& itemHit.collider.GetComponent<WeaponScript>() != null)
		{
			WeaponScript weaponScript = itemHit.collider.GetComponent<WeaponScript>();
			Global.Instance.HUDManager.defaultItemCard.gameObject.SetActive(true);
			Global.Instance.HUDManager.defaultItemCard.UpdateItemCard(weaponScript);
			Global.Instance.HUDManager.defaultItemCard.UpdatePosition(Input.mousePosition);
		}else{
			Global.Instance.HUDManager.defaultItemCard.gameObject.SetActive(false);
		}
	}
}
