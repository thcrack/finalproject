﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	Transform target;
	public bool isFollowingPlayer = true;
	public Transform playerTarget;
	public Transform freeTarget;
	public bool fixedMode = true;
	public float smoothing = 5f;
	public float maxMouseStretchingRadius = 250f;
	public float mouseStretchRatio = 0.012f;
	public float shiftModifier = 2f;

	public float offsetX, offsetY, offsetZ;

	Vector3 screenCenter;
	Vector3 offset;

	void Start(){
		offset.Set(offsetX, offsetY, offsetZ);
		screenCenter = new Vector3(Screen.width/2,Screen.height/2,0);
		if(isFollowingPlayer){
			target = playerTarget;
		}else{
			target = freeTarget;
		}
	}

	void Update(){
		if(!fixedMode) CameraUpdate();
	}

	void FixedUpdate(){
		if(fixedMode) CameraUpdate();
	}

	void CameraUpdate(){

		Vector3 mouseRelation = Input.mousePosition - screenCenter;

		float radius = Mathf.Min(mouseRelation.magnitude, maxMouseStretchingRadius);
		mouseRelation = mouseRelation.normalized * radius;
		mouseRelation.Set(mouseRelation.x, 0, mouseRelation.y);

		if(Input.GetButton("Shift")) mouseRelation *= shiftModifier;
		if(!fixedMode) mouseRelation = Vector3.zero;

		Vector3 targetCamPos = target.position + offset + mouseRelation * mouseStretchRatio;
		transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);

	}

	public void Shake(float force){
		Vector3 shakeOffset = target.rotation * Vector3.forward * -force;
		transform.position += shakeOffset;
	}

	public void ToggleFollowMode(){
		if(!isFollowingPlayer){
			target = playerTarget;
			fixedMode = true;
			isFollowingPlayer = true;
		}else{
			target = freeTarget;
			fixedMode = false;
			isFollowingPlayer = false;
		}
	}

}
