﻿using UnityEngine;
using System.Collections;

namespace UnityStandardAssets.ImageEffects
{
	public class CameraBlurBackground : MonoBehaviour {

		BlurOptimized blur;
		GameManager gameManager;

		[Range(0.0f, 10.0f)]
		public float BlurIntensity = 5f;

		// Use this for initialization
		void Start () {
			blur = GetComponent<BlurOptimized>();
			gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		}
		
		// Update is called once per frame
		void Update () {
			if(gameManager.pauseState){
				blur.enabled = true;
				blur.blurSize = Mathf.Lerp(blur.blurSize, BlurIntensity, 0.5f * Time.fixedDeltaTime);
			}else{
				blur.enabled = false;
				blur.blurSize = 0f;
			}
		}
	}
}
