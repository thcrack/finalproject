﻿using UnityEngine;
using System.Collections;

public class CameraEffectManager : MonoBehaviour {

	CameraFilterPack_Color_Chromatic_Aberration colorFilter;
	CameraFilterPack_TV_WideScreenHorizontal widescreenFilter;

	float hurtTimer;
	public float hurtEffectTime = 0.1f;

	float widescreenTimer;
	public bool isWidescreen = false;
	public float widescreenDestination = 0.5f;
	public float widescreenFadeTime = 1f;

	void Start(){

		colorFilter = GetComponent<CameraFilterPack_Color_Chromatic_Aberration>();
		widescreenFilter = GetComponent<CameraFilterPack_TV_WideScreenHorizontal>();

	}

	void Update(){

		if(hurtTimer > 0){

			hurtTimer -= Time.deltaTime;
			colorFilter.Offset = 0.01f * hurtTimer / hurtEffectTime;

		}else{

			colorFilter.enabled = false;

		}

		if(widescreenTimer > 0f){

			widescreenTimer -= Time.deltaTime;

			if(isWidescreen){
				widescreenFilter.Size = Mathf.Lerp(widescreenDestination, 0.8f, widescreenTimer / widescreenFadeTime);
			}else{
				widescreenFilter.Size = Mathf.Lerp(0.8f, widescreenDestination, widescreenTimer / widescreenFadeTime);
				if(widescreenTimer <= 0f) widescreenFilter.enabled = false;
			}
		}

	}

	public void PlayerHurt(){
		colorFilter.enabled = true;
		colorFilter.Offset = 0.01f;

		hurtTimer = hurtEffectTime;
	}

	public void EnterWidescreen(){

		if(widescreenTimer > 0f) return;

		isWidescreen = true;
		Global.Instance.CinemaMode = true;
		widescreenFilter.enabled = true;
		widescreenTimer = widescreenFadeTime;

	}

	public void ExitWidescreen(){

		if(widescreenTimer > 0f) return;

		isWidescreen = false;
		Global.Instance.CinemaMode = false;
		widescreenTimer = widescreenFadeTime;

	}

	public void Trigger(){
		if(!isWidescreen){
			EnterWidescreen();
		}else{
			ExitWidescreen();
		}
	}
}
