﻿using UnityEngine;
using System.Collections;

public class BotCommandManager : MonoBehaviour {

	public BotDestination [] Destinations;
	
	void Update () {

		if(Input.GetButtonDown("Command1")){
			Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast(camRay, out hit, 100f, LayerMask.GetMask("Interactive"), QueryTriggerInteraction.Collide)){

				Debug.Log(hit.collider.gameObject.name);
				Destinations[0].transform.position = hit.collider.GetComponent<InteractiveScript>().BotDestination.position;
				Destinations[0].SetTargetInteractive(hit.collider.gameObject);
				Destinations[0].CancelTargetWeapon();
				Destinations[0].CancelTargetEnemy();

			}else if(Physics.Raycast(camRay, out hit, 100f)) {
				Destinations[0].CancelTargetInteractive();
				if(hit.collider.gameObject.tag == "Enemy"){
					Destinations[0].SetTargetEnemy(hit.collider.gameObject);
					Destinations[0].CancelTargetWeapon();
				}else if(hit.collider.gameObject.layer == LayerMask.NameToLayer("Item")){
					Destinations[0].transform.position = hit.point;
					Destinations[0].SetTargetWeapon(hit.collider.gameObject);
				}else if(hit.collider.gameObject.layer == LayerMask.NameToLayer("Floor")
						||	Physics.Raycast(camRay, out hit, 100f, LayerMask.GetMask("Floor"))){
					Destinations[0].transform.position = hit.point;
					Destinations[0].CancelTargetWeapon();
				}
			}
		}

		if(Input.GetButtonDown("Command2")){
			Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(camRay, out hit, 100f, LayerMask.GetMask("Interactive"), QueryTriggerInteraction.Collide)){

				Destinations[1].transform.position = hit.collider.GetComponent<InteractiveScript>().BotDestination.position;
				Destinations[1].SetTargetInteractive(hit.collider.gameObject);
				Destinations[1].CancelTargetWeapon();
				Destinations[1].CancelTargetEnemy();

			}else if (Physics.Raycast(camRay, out hit, 100f)) {
				Destinations[1].CancelTargetInteractive();
				if(hit.collider.gameObject.tag == "Enemy"){
					Destinations[1].SetTargetEnemy(hit.collider.gameObject);
					Destinations[1].CancelTargetWeapon();
				}else if(hit.collider.gameObject.layer == LayerMask.NameToLayer("Item")){
					Destinations[1].transform.position = hit.point;
					Destinations[1].SetTargetWeapon(hit.collider.gameObject);
				}else if(hit.collider.gameObject.layer == LayerMask.NameToLayer("Floor")
						||	Physics.Raycast(camRay, out hit, 100f, LayerMask.GetMask("Floor"))){
					Destinations[1].transform.position = hit.point;
					Destinations[1].CancelTargetWeapon();
				}
			}
		}

		if(Input.GetButtonDown("Command3")){
			Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(camRay, out hit, 100f, LayerMask.GetMask("Interactive"), QueryTriggerInteraction.Collide)){

				Destinations[2].transform.position = hit.collider.GetComponent<InteractiveScript>().BotDestination.position;
				Destinations[2].SetTargetInteractive(hit.collider.gameObject);
				Destinations[2].CancelTargetWeapon();
				Destinations[2].CancelTargetEnemy();

			}else if (Physics.Raycast(camRay, out hit, 100f)) {
				Destinations[2].CancelTargetInteractive();
				if(hit.collider.gameObject.tag == "Enemy"){
					Destinations[2].SetTargetEnemy(hit.collider.gameObject);
					Destinations[2].CancelTargetWeapon();
				}else if(hit.collider.gameObject.layer == LayerMask.NameToLayer("Item")){
					Destinations[2].transform.position = hit.point;
					Destinations[2].SetTargetWeapon(hit.collider.gameObject);
				}else if(hit.collider.gameObject.layer == LayerMask.NameToLayer("Floor")
						||	Physics.Raycast(camRay, out hit, 100f, LayerMask.GetMask("Floor"))){
					Destinations[2].transform.position = hit.point;
					Destinations[2].CancelTargetWeapon();
				}
			}
		}
	
	}
}
