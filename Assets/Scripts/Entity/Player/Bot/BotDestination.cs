﻿using UnityEngine;
using System.Collections;

public class BotDestination : MonoBehaviour {

	public GameObject TargetInteractive;
	public GameObject TargetWeapon;
	public GameObject TargetEnemy;

	public void SetTargetInteractive(GameObject input){
		TargetInteractive = input;
	}

	public void SetTargetWeapon(GameObject input){
		TargetWeapon = input;
	}

	public void SetTargetEnemy(GameObject input){
		TargetEnemy = input;
	}

	public void CancelTargetInteractive(){
		TargetInteractive = null;
	}

	public void CancelTargetWeapon(){
		TargetWeapon = null;
	}

	public void CancelTargetEnemy(){
		TargetEnemy = null;
	}
}
