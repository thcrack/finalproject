﻿using UnityEngine;
using System.Collections;

public class PlayerBehavior : EntityStat {

	[System.SerializableAttribute]
	public class PlayerMovement{

		[Header("General Parameters")]
		public float interactionRange = 2f;
		public float speed = 7f;
		public float acceleration = 10f;
		public float angularSpeed = 720f;


		[Header("Bot Parameters")]
		public float MaxTargetDetectRange = 10f;
		public float attackReactionTime = 0.3f;

	}

	public class PlayerAIState{
		public PlayerAIStateFollow followState;
		public PlayerAIStateMove moveState;
		public PlayerAIStateInteract interactState;
		public PlayerAIStateChase chaseState;

		public IPlayerAIState currentState;

		public PlayerAIState(PlayerBehavior player){	
	        followState = new PlayerAIStateFollow(player);
	        moveState = new PlayerAIStateMove(player);
	        interactState = new PlayerAIStateInteract(player);
	        chaseState = new PlayerAIStateChase(player);
		}
	}

	public string playerName = "Player";
    public bool isBot = false;
    public bool concealStance = false;
    public Material concealMaterial;
    public Transform weaponPosition;

    Vector3 secondaryVelocity;

    GameObject equippedWeaponObject;

    [HideInInspector] public UnityEngine.AI.NavMeshAgent nav;

    [HideInInspector] public bool isSelected;
    [HideInInspector] public EntityStat enemyTarget;
    [HideInInspector] public InteractiveScript interactiveTarget;
    [HideInInspector] public Vector3 movingTarget;

    InteractiveScript currentInteractive;
    bool isInteracting;
    public PlayerMovement playerMovement;
    public PlayerAIState playerAIState;

    WeaponScript nearbyWeapon;

    Renderer rend;
    Material originalMat;
    LineRenderer line;
    
    float reactionTimer;
    float interactTimer;
    int floorMask, wallMask, floorLayer, wallLayer;

    Vector3 gunPointHeightOffset;

    [HideInInspector] public Rigidbody rigidbody;

	void Awake()
	{
		currentHealth = entityAttribute.maxHealth;

		secondaryVelocity = Vector3.zero;

		rend = GetComponent<Renderer>();
		originalMat = rend.material;

		line = GetComponent<LineRenderer>();

        entityAttribute.faction = EntityFaction.Player;
        rigidbody = GetComponent<Rigidbody>();
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        nav.updatePosition = false;
        nav.speed = playerMovement.speed;
        nav.angularSpeed = playerMovement.angularSpeed;

        if(isBot)
        {
        	nav.enabled = true;
            nav.destination = transform.position + new Vector3(nav.stoppingDistance, 0f, 0f);
        }else{
        	nav.enabled = false;
        }

        playerAIState = new PlayerAIState(this);
        playerAIState.currentState = playerAIState.followState;

        floorMask = LayerMask.GetMask("Floor");
        wallMask = LayerMask.GetMask("Wall");
        floorLayer = LayerMask.NameToLayer("Floor");
        wallLayer = LayerMask.NameToLayer("Wall");

        gunPointHeightOffset = new Vector3(0f, weaponPosition.localPosition.y, 0f);

        equippedWeaponObject = (GameObject) Instantiate(equippedWeapon.gameObject, weaponPosition);
		equippedWeaponObject.transform.localPosition = Vector3.zero;
		equippedWeaponObject.transform.localRotation = Quaternion.identity;
		equippedWeapon = equippedWeaponObject.GetComponent<WeaponScript>();
		equippedWeapon.SetEquipped(true, this);
		equippedWeapon.LoadClip();

		if(concealStance){
			equippedWeapon.gameObject.SetActive(false);
			rend.sharedMaterial = concealMaterial;
			GetComponentInChildren<Light>().enabled = false;
		}
	}

	void FixedUpdate(){

		if(entityAttribute.isDead || Global.Instance.CinemaMode) return;

		if(secondaryVelocity != Vector3.zero){

            float mag = secondaryVelocity.magnitude;

            if(mag >= 15f) secondaryVelocity = secondaryVelocity.normalized * 15f;

            if(mag <= playerMovement.acceleration * Time.fixedDeltaTime){
                secondaryVelocity = Vector3.zero;
            }else{
                secondaryVelocity -= secondaryVelocity.normalized * playerMovement.acceleration * Time.fixedDeltaTime;
            }
        }

		if(isBot){

			nav.nextPosition = transform.position;

			if(nav.enabled) playerAIState.currentState.FixedUpdateState();

			// for(int i = 0; i < nav.path.corners.Length -1; i++){
			// 	Debug.DrawLine(nav.path.corners[i], nav.path.corners[i+1]);
			// }
			
			if(nav.path.status == UnityEngine.AI.NavMeshPathStatus.PathComplete && nav.remainingDistance > nav.stoppingDistance && nav.path.corners.Length >= 2)
			{
				NavMove(nav.path.corners[1]);
			}else{
                KnockbackMove();
            }

		}else{

			float h = Input.GetAxisRaw ("Horizontal");
			float v = Input.GetAxisRaw ("Vertical");

			if(!isInteracting){
				Move (h, v);
				Turning ();
			}else{
				KnockbackMove();
			}

		}

	}

	void Update(){

		if(entityAttribute.isDead || Global.Instance.CinemaMode) return;

		ApplyBuffEffects();

		if(isBot){

			playerAIState.currentState.UpdateState();
			if(nav.path.corners.Length > 1 && nav.remainingDistance > nav.stoppingDistance + 0.2f){
				line.enabled = true;
				line.SetVertexCount(nav.path.corners.Length);
				for(int i = 0; i < nav.path.corners.Length; i++){
					Vector3 offSetpos = nav.path.corners[i];
					offSetpos.y += 0.05f;
					line.SetPosition(i, offSetpos);
				}
			}else{
				line.enabled = false;
			}


		}else{

			InteractiveScript interactiveCandidate = null;

			if(currentInteractive == null){

				Collider[] interactiveColliders = Physics.OverlapSphere(transform.position, playerMovement.interactionRange, LayerMask.GetMask("Interactive"), QueryTriggerInteraction.Collide);

				if(interactiveColliders != null && interactiveColliders.Length > 0){
					interactiveCandidate = interactiveColliders[0].GetComponent<InteractiveScript>();
				}

			}else{

				if(currentInteractive.Interactable){

					if(interactTimer <= 0){

						currentInteractive.Interact(gameObject);
						currentInteractive = null;
						DisableInteraction();

					}else{

						interactTimer -= Time.deltaTime;
						Vector3 relative = currentInteractive.transform.position - transform.position;
						relative.Set(relative.x, 0f, relative.z);
						transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(relative), playerMovement.angularSpeed * Time.deltaTime);

					}

				}else{

					DisableInteraction();

				}
				
			}

			Collider[] itemCol = Physics.OverlapSphere(transform.position, playerMovement.interactionRange, LayerMask.GetMask("Item"), QueryTriggerInteraction.Ignore);
			if(itemCol.Length > 0) nearbyWeapon = itemCol[0].GetComponent<WeaponScript>();

			if(Input.GetButtonDown("Use")){
				if(isInteracting){

					DisableInteraction();

				}else if(interactiveCandidate != null){

					equippedWeapon.StopReloading();

					isInteracting = true;
					currentInteractive = interactiveCandidate;
					interactTimer = currentInteractive.InteractionTime;

				}else if(nearbyWeapon != null){

					Global.Instance.InventoryManager.AddWeapon(nearbyWeapon.gameObject);

				}
			}

			if(Input.GetButton ("Fire1")){

				DisableInteraction();
				FireWeapon();
			}

			if(Input.GetButton ("Reload")){

				DisableInteraction();
				ReloadWeapon();
			}
		}
	}

	void DisableInteraction(){
		isInteracting = false;
		currentInteractive = null;
	}

	public void EquipWeapon(WeaponScript weapon){
		equippedWeapon = weapon;
		equippedWeaponObject = weapon.gameObject;
		equippedWeaponObject.SetActive(true);
		equippedWeaponObject.transform.SetParent(weaponPosition);
		equippedWeaponObject.transform.localPosition = Vector3.zero;
		equippedWeaponObject.transform.localRotation = Quaternion.identity;
		equippedWeapon.SetEquipped(true, this);
	}

	public override void Knockback(Vector3 point, Vector3 dir, float force){

        dir = dir.normalized * force;
        secondaryVelocity += dir / rigidbody.mass;
        if(entityAttribute.isDead) rigidbody.AddForceAtPosition(dir, point, ForceMode.Impulse);

    }

	public override void Death()
	{
		base.Death();

        Global.Instance.PlayerManager.RemoveFromAliveList(this);


        rigidbody.constraints = RigidbodyConstraints.None;
        rigidbody.AddForce(Vector3.back * 100f, ForceMode.Impulse);
        GetComponentInChildren<Light>().enabled = false;
        GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
        equippedWeapon.SetEquipped(false, null);
        equippedWeapon = null;
        equippedWeaponObject = null;
	}

	void Move(float h, float v){

		Vector3 originalSpeed = rigidbody.velocity;
		Vector3 movement = new Vector3(h, 0f, v);
		float concealMod = 1f;
		if(concealStance) concealMod = 0.75f;
		movement = movement.normalized * playerMovement.speed * concealMod + secondaryVelocity;
		movement.Set(movement.x, originalSpeed.y, movement.z);
		rigidbody.velocity = movement;

	}

	void NavMove(Vector3 dest){

		Vector3 originalSpeed = rigidbody.velocity;
		Vector3 movement = dest - transform.position;
		float brake = Mathf.Min(1f, nav.remainingDistance/nav.stoppingDistance - 0.5f);
		float concealMod = 1f;
		if(concealStance) concealMod = 0.75f;
		movement = movement.normalized * playerMovement.speed * brake * concealMod + secondaryVelocity;
		movement.Set(movement.x, originalSpeed.y, movement.z);
		rigidbody.velocity = movement;

	}

	void KnockbackMove(){

		Vector3 originalSpeed = rigidbody.velocity;
		Vector3 movement = secondaryVelocity;
		movement.Set(movement.x, originalSpeed.y, movement.z);
		rigidbody.velocity = movement;

	}

	void OnCollisionEnter(Collision col){
		if(col.collider.gameObject.layer != floorLayer && secondaryVelocity != Vector3.zero){
			Vector3 normal = col.contacts[0].normal;
			if(col.collider.gameObject.layer == wallLayer){
                secondaryVelocity = Vector3.Reflect(secondaryVelocity, normal) * 0.6f;
            }else{
                secondaryVelocity *= 0.8f;
            }
		}
	}

	void Turning(){
		Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit floorHit;
		if (Physics.Raycast (camRay, out floorHit, 100f, floorMask)) {
			Vector3 playerToMouse = floorHit.point - transform.position;
			playerToMouse.y = 0f;
			Quaternion newRotation = Quaternion.LookRotation (playerToMouse);
			rigidbody.MoveRotation (Quaternion.RotateTowards(transform.rotation, newRotation, playerMovement.angularSpeed * Time.deltaTime));
		}
	}

	public EntityStat GetEnemyTarget()
	{
		return enemyTarget;
	}

	public float GetInteractionProgress(){

		if(isInteracting){
			return 1f - (interactTimer / currentInteractive.InteractionTime);
		}
		if(concealStance) return -1f;
		if(equippedWeapon != null){
			return equippedWeapon.GetProgress();
		}

		return -1f;
	}

	public void AcquireEnemyTarget()
	{

		float detectRange = Mathf.Min(equippedWeapon.moddedWeaponProperty.weaponRange, playerMovement.MaxTargetDetectRange);
	        	
    	Collider[] enemyInRange = Physics.OverlapSphere(transform.position, detectRange);

    	if(enemyInRange.Length > 0){

    		float bestDist = Mathf.Infinity;
    		EntityStat bestTarget = null;

        	foreach(Collider col in enemyInRange){
        		GameObject obj = col.gameObject;

        		// Check health
        		EntityStat entity = obj.GetComponent<EntityStat>();
				if(entity == null || entity.entityAttribute.faction != EntityFaction.Enemy || entity.entityAttribute.isDead){
					continue;
				}

				// Check if closer than current candidate

				float dist = Vector3.Distance(obj.transform.position, transform.position);
				if(dist < bestDist){

					if(hasLineOfSight(obj.transform)){

						bestDist = dist;
						bestTarget = obj.GetComponent<EntityStat>();

					}
					
				}

        	}

        	if(bestTarget != null ) enemyTarget = bestTarget;

        }

    	reactionTimer = 0;

	}

	public bool Aim()
	{
		bool isAimed = false;

		if(enemyTarget == null)
		{

			nav.angularSpeed = playerMovement.angularSpeed;
			AcquireEnemyTarget();

		}else if(hasLineOfSight(enemyTarget.transform)){

			if ((equippedWeapon.baseWeaponProperty.weaponType != WeaponType.Melee && reactionTimer >= playerMovement.attackReactionTime)
			|| ( equippedWeapon.baseWeaponProperty.weaponType == WeaponType.Melee
				&& Vector3.Distance(transform.position, enemyTarget.transform.position) <= equippedWeapon.moddedWeaponProperty.weaponRange)) isAimed = true;

			nav.angularSpeed = 0;

			Vector3 relative = enemyTarget.transform.position - transform.position;
			transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(relative), playerMovement.angularSpeed * Time.deltaTime);

			reactionTimer += Time.deltaTime;

		}else{

			if(playerAIState.currentState != playerAIState.chaseState) enemyTarget = null;
			nav.angularSpeed = playerMovement.angularSpeed;

		}

		return isAimed;
	}

	public void FireWeapon()
	{
		if(!concealStance && equippedWeapon != null) equippedWeapon.Fire();
	}

	public void ReloadWeapon()
	{
		if(!concealStance && equippedWeapon != null) equippedWeapon.Reload();
	}

	bool hasLineOfSight(Transform target)
	{
		if(target.tag == "Ragdoll") return false;

		RaycastHit visibleHit;
		Ray visibleRay = new Ray();
		visibleRay.origin = transform.position + gunPointHeightOffset;
		Vector3 fixedPos = new Vector3 (target.position.x, transform.position.y, target.position.z);
		visibleRay.direction = fixedPos - transform.position;

		return !Physics.Raycast(visibleRay, out visibleHit, Vector3.Distance(transform.position, target.position), wallMask, QueryTriggerInteraction.Ignore);
	}

	public void CancelConceal(){
		concealStance = false;
		equippedWeapon.gameObject.SetActive(true);
		rend.material = originalMat;
		GetComponentInChildren<Light>().enabled = true;
	}
}
