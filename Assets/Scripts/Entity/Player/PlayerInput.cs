﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour {

	public float _speed = 5f;
	public float _rotateSpeed = 7f;
	public Camera _camera;

	Rigidbody _rigidbody;
	Vector3 _movement;

	void Awake(){
		_rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame

	void FixedUpdate(){
		float h = Input.GetAxisRaw ("Horizontal");
		float v = Input.GetAxisRaw ("Vertical");

		float rh = Input.GetAxis("Mouse X");
		float rv = Input.GetAxis("Mouse Y");

		Move (h, v);
		Rotate(rh, rv);
	}

	void Move(float h, float v){
		Quaternion currentAngle = _camera.transform.rotation;
		currentAngle.eulerAngles = new Vector3(currentAngle.eulerAngles.x, currentAngle.eulerAngles.y, 0);
		_movement.Set(h,0f,v);
		_movement = _movement.normalized * _speed * Time.deltaTime;
		_movement = currentAngle * _movement;
		_rigidbody.MovePosition(transform.position + _movement);
	}

	void Rotate(float h, float v){

		_camera.transform.localEulerAngles += new Vector3(-v * _rotateSpeed, h * _rotateSpeed,0);
	}
}
