using UnityEngine;

public class PlayerCameraFollow : MonoBehaviour {

	public Camera _camera;
	
	void Awake(){
		_camera.transform.rotation = transform.rotation;
	}

	// Update is called once per frame
	void Update () {
		_camera.transform.position = transform.position;
		_camera.transform.localEulerAngles = new Vector3(_camera.transform.localEulerAngles.x,_camera.transform.localEulerAngles.y,0);
	}
}
