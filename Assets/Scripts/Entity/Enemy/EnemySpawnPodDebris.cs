﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawnPodDebris : MonoBehaviour {

	public List<Rigidbody> rigidbodies;
	public float explosionForce = 50f;
	public float explosionRadius = 15f;
	public float centerOffsetY = 2f;
	public float centerRandomOffsetXZRange = 0.5f;

	// Use this for initialization
	void Awake () {

		Vector3 center = transform.position;
		center.Set(center.x + Random.Range(-centerRandomOffsetXZRange, centerRandomOffsetXZRange), center.y + centerOffsetY, center.z + Random.Range(-centerRandomOffsetXZRange, centerRandomOffsetXZRange));

		foreach(Rigidbody rgd in rigidbodies){
			rgd.AddExplosionForce(explosionForce, center, explosionRadius, 0f, ForceMode.Impulse);
		}
	
	}

}
