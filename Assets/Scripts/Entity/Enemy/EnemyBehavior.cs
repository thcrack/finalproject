using UnityEngine;
using System.Collections;

public enum EnemyType {
    Common, Special, Legend, Boss, RaidBoss, Pod
}

public class EnemyBehavior : EntityStat {

	public EntityStat targetEntity;

    public EnemyType enemyType = EnemyType.Common;

    [System.SerializableAttribute]
	public class EnemyMovement{
		public float speed = 2.5f;
        public float acceleration = 6f;
	    [Range(0,0.5f)]
	    public float speedOffsetPercentage = 0.2f;

        public bool isFreezed = false;
        public float freezeTimeAfterAttack = 0.2f;
	}

    public Transform weaponPosition;

    GameObject equippedWeaponObject;
    
	public Material EnemyDeathMaterial;

	public EnemyMovement enemyMovement;

	Rigidbody rigidbody;
    Vector3 secondaryVelocity;

	[HideInInspector] public UnityEngine.AI.NavMeshAgent nav;

	float freezeTimer;
    float realSpeed;
    float weaponPointOffset = 0.25f;

    AudioSource audio;

	void Awake()
	{
        entityAttribute.faction = EntityFaction.Enemy;
		currentHealth = entityAttribute.maxHealth;
		rigidbody = GetComponent<Rigidbody>();
        secondaryVelocity = Vector3.zero;
		nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        nav.updatePosition = false;
        realSpeed = enemyMovement.speed * (1f + enemyMovement.speedOffsetPercentage * Random.Range(-1f,1f));

        audio = GetComponent<AudioSource>();

        equippedWeaponObject = (GameObject) Instantiate(equippedWeapon.gameObject, weaponPosition);
        equippedWeaponObject.transform.localPosition = Vector3.zero;
        equippedWeaponObject.transform.localRotation = Quaternion.identity;
        equippedWeapon = equippedWeaponObject.GetComponent<WeaponScript>();
        equippedWeapon.SetEquipped(true, this);
        equippedWeapon.LoadClip();
        nav.stoppingDistance = equippedWeapon.moddedWeaponProperty.weaponRange + weaponPointOffset;
	}

	void FixedUpdate ()
    {

        nav.nextPosition = transform.position;

        if(secondaryVelocity != Vector3.zero){

            float mag = secondaryVelocity.magnitude;

            if(mag <= enemyMovement.acceleration * Time.fixedDeltaTime){
                secondaryVelocity = Vector3.zero;
            }else{
                secondaryVelocity -= secondaryVelocity.normalized * enemyMovement.acceleration * Time.fixedDeltaTime;
            }
        }

        if(targetEntity != null && currentHealth > 0 && targetEntity.currentHealth > 0)
        {
        	nav.destination = targetEntity.transform.position;
            if(nav.remainingDistance <= nav.stoppingDistance) transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation (targetEntity.transform.position - transform.position), nav.angularSpeed * Time.deltaTime);
            if(nav.path.status == UnityEngine.AI.NavMeshPathStatus.PathComplete && nav.path.corners.Length >= 2)
            {
                NavMove(nav.path.corners[1]);
            }else{
                KnockbackMove();
            }
        }
    }

    void Update(){

        freezeTimer -= Time.deltaTime;

        ApplyBuffEffects();

        if(PlayerInRange() && currentHealth > 0)
        {
            equippedWeapon.Fire();

            enemyMovement.isFreezed = true;
            freezeTimer = enemyMovement.freezeTimeAfterAttack;

        }else if(freezeTimer <= 0f){

            enemyMovement.isFreezed = false;

        }

        if(targetEntity != null && targetEntity.entityAttribute.isDead)
        {
            if(!Global.Instance.PlayerManager.CheckIsAllDead()){
                FindRandomPlayer();
            }else{
                nav.enabled = false;
            }
        }

    }

    bool PlayerInRange()
    {
    	return targetEntity != null && Vector3.Distance(targetEntity.transform.position, transform.position) <= equippedWeapon.moddedWeaponProperty.weaponRange + weaponPointOffset;
    }

    public void FindRandomPlayer(){
        targetEntity = Global.Instance.PlayerManager.GetRandomAlivePlayer();
    }

    void OnCollisionEnter (Collision col){
        if(gameObject.tag != "Ragdoll" && col.collider.gameObject.tag == "Ragdoll"){
            Physics.IgnoreCollision(col.collider, GetComponent<Collider>());
        }else if(secondaryVelocity != Vector3.zero){
            Vector3 normal = col.contacts[0].normal;
            if(col.collider.gameObject.layer == LayerMask.NameToLayer("Wall")){
                secondaryVelocity = Vector3.Reflect(secondaryVelocity, normal) * 0.6f;
            }else{
                secondaryVelocity *= 0.8f;
            }
        }
    }

    void NavMove(Vector3 dest){

        Vector3 originalSpeed = rigidbody.velocity;
        Vector3 movement = dest - transform.position;

        float freezeMod = (enemyMovement.isFreezed) ? 0f : 1f;

        // float brake = Mathf.Min(1f, nav.remainingDistance/nav.stoppingDistance - 0.5f);
        movement = movement.normalized * realSpeed * freezeMod * entityBuffMod.movementMod + secondaryVelocity;
        movement.Set(movement.x, originalSpeed.y, movement.z);
        rigidbody.velocity = movement;

    }

    void KnockbackMove(){

        Vector3 originalSpeed = rigidbody.velocity;
        Vector3 movement = secondaryVelocity;
        movement.Set(movement.x, originalSpeed.y, movement.z);
        rigidbody.velocity = movement;

    }

    public override void Hit(HitInfo dmgInfo, bool applyBuff){
        base.Hit(dmgInfo, applyBuff);

        audio.Play();

        if(Vector3.Distance(transform.position, dmgInfo.Damager.transform.position)
            <= Vector3.Distance(transform.position, targetEntity.transform.position) * 0.5f
        && Random.Range(0f, 1f) < 0.1f)
        {
            targetEntity = dmgInfo.Damager;
        }
    }

	public override void Knockback(Vector3 point, Vector3 dir, float force){
            dir = dir.normalized * force;
            secondaryVelocity += dir / rigidbody.mass;
            if(entityAttribute.isDead) rigidbody.AddForceAtPosition(dir, point, ForceMode.Impulse);
    }

	public override void Death()
	{
		base.Death();
		Renderer rend = GetComponent<Renderer>();
        rend.sharedMaterial = EnemyDeathMaterial;

        GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
        //GetComponentInChildren<Light>().enabled = false;

        Destroy(equippedWeaponObject);

        Global.Instance.WeaponSpawnManager.RollWeaponOnDeath(transform.position, enemyType);

        rigidbody.constraints = RigidbodyConstraints.None;
	}
}
