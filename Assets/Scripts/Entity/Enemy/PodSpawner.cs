using UnityEngine;
using System.Collections;

public class PodSpawner : MonoBehaviour{

	public bool hasDropped = false;

	public EnemySpawnPod podPrefab;
	public int spawnQuota = 30;
	public float spawnInterval = 0.3f;
	public float podDropHeight = 25f;

	GameObject newPod;
	EnemySpawnPod newPodScript;

	Vector3 newPodVelocity;

	void Awake(){

		Vector3 dropPosition = transform.position + new Vector3(0f, podDropHeight, 0f);

		newPod = (GameObject) Instantiate(podPrefab.gameObject, dropPosition, transform.rotation);

		newPodScript = newPod.GetComponent<EnemySpawnPod>();
		newPodScript.SetSpawnQuota(spawnQuota);
		newPodScript.SetSpawnInterval(spawnInterval, false);

		newPod.SetActive(false);

		newPodVelocity = Vector3.zero;
	}

	void FixedUpdate(){
		if(hasDropped){
			newPod.transform.position += newPodVelocity * Time.fixedDeltaTime;
			if(newPod.transform.position.y < transform.position.y){
				newPod.transform.position = transform.position;
				newPod.SendMessage("Trigger");
				Destroy(gameObject);
			}else{
				newPodVelocity += Physics.gravity * Time.fixedDeltaTime;
			}
		}
	}

	public void Trigger(){

		hasDropped = true;
		newPod.SetActive(true);

	}

}