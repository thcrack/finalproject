﻿using UnityEngine;
using System.Collections;

public class EnemySpawnPod : EntityStat {

    public bool Enabled = true;
    public GameObject enemy;
    public GameObject debrisPrefab;
    public Transform SpawnPoint;
    public float PodTopLightFlashRange = 5f;
    public int MaxSpawnCount = 5;
    public float spawnTime = 3f;
    public Material DisabledMaterial;

    Material OriginalMaterial;
    Renderer rend;

    int spawnQuota;
    [SerializeField] Light PodInnerLight;
    [SerializeField] Light PodTopLight;

    float TopRange;


    void Awake ()
    {
        currentHealth = entityAttribute.maxHealth;

        rend = GetComponent<Renderer>();

        OriginalMaterial = rend.material;
        spawnQuota = MaxSpawnCount;

        if(Enabled && spawnQuota > 0){

            InvokeRepeating ("Spawn", 0f, spawnTime);

        }else{

            PodInnerLight.enabled = false;
            PodTopLight.enabled = false;
            
            rend.material = DisabledMaterial;

        }

        TopRange = PodTopLight.range;
    }

    void Update(){
        if(PodTopLight.range > TopRange + 0.1f){
            PodTopLight.range = Mathf.Lerp(PodTopLight.range, TopRange, 10f * Time.deltaTime);
        }else{
            PodTopLight.range = TopRange;
            if(!Enabled) PodTopLight.enabled = false;
        }
    }


    void Spawn ()
    {
        if(Global.Instance.PlayerManager.CheckIsAllDead()){
            return;
        }

        spawnQuota--;
        PodTopLight.range = PodTopLightFlashRange;

        GameObject newEnemy = (GameObject) Instantiate (enemy, SpawnPoint.position, SpawnPoint.rotation);
        newEnemy.transform.SetParent(Global.Instance.EnemyManager);
        newEnemy.GetComponent<EnemyBehavior>().FindRandomPlayer();

        if(spawnQuota == 0){
            CancelInvoke();
            Enabled = false;
            PodInnerLight.enabled = false;
            rend.material = DisabledMaterial;
        }
    }

    public void SetSpawnQuota(int amount){
        spawnQuota = amount;
    }

    public void SetSpawnInterval(float interval, bool changeCurrentSpawnRate){
        spawnTime = interval;
        if(changeCurrentSpawnRate){
            CancelInvoke("Spawn");
            InvokeRepeating("Spawn", 0f, spawnTime);
        }
    }

    void Trigger(){
        if(spawnQuota > 0){
            Enabled = true;
            InvokeRepeating ("Spawn", 0f, spawnTime);
            PodInnerLight.enabled = true;
            PodTopLight.enabled = true;
            rend.material = OriginalMaterial;
        }
    }

    public override void Death()
    {
        entityAttribute.isDead = true;
        Instantiate(debrisPrefab, transform.position, transform.rotation);
        Global.Instance.WeaponSpawnManager.RollWeaponOnDeath(transform.position, EnemyType.Pod);
        Destroy(gameObject);
    }
}
