﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum EntityFaction{
    Player,
    Enemy,
    Neutral
}

public class EntityStat : MonoBehaviour {

    [System.SerializableAttribute]
    public class EntityAttribute
    {

        public EntityFaction faction = EntityFaction.Neutral;
        public bool isDead = false;
        public float maxHealth = 100f;

    }

    [System.SerializableAttribute]
    public struct EntityBuffMod
    {

        public float movementMod;

    }

    [System.SerializableAttribute]
    public class EntityResistance
    {
        
        [Header("Note: RAD Resistance and armor are always ignored.")]
        public float PierceResistance = 1f;
        [Header("Flat Resistance (%)")]
        public DamageInfo FlatResistance;
        [Header("Armor Value - Related to eHP")]
        public DamageInfo ArmorValue;
        [HideInInspector] public bool valueUpToDate = false;
        [HideInInspector] public DamageInfo calculatedValue;
    }

    public EntityAttribute entityAttribute;
    public EntityResistance entityResistance;
    public EntityBuffMod entityBuffMod;

    public WeaponScript equippedWeapon;
    public List<BuffBehavior> buffList;

    public float currentHealth;

	public virtual void Hit (HitInfo dmgInfo, bool applyBuff)
    {

        if(entityAttribute.isDead) return;

        if(!entityResistance.valueUpToDate) CalculateResistanceValue();

        currentHealth -= dmgInfo.GetDamage();

        if(applyBuff){

            List<BuffApplication> buffAppList = dmgInfo.GetBuffOnHit();

            if(buffAppList != null){

                foreach(BuffApplication buffApp in buffAppList){

                    ApplyBuff(buffApp, dmgInfo.Damager);

                }

            }

        }

        if(entityAttribute.faction == EntityFaction.Player) Camera.main.GetComponent<CameraEffectManager>().PlayerHurt();

        if(currentHealth <= 0)
        {
            Death();
        }
    }

    public void Hit (float dmg)
    {

        if(entityAttribute.isDead) return;

        currentHealth -= dmg;

        if(currentHealth <= 0)
        {
            Death();
        }
    }

    virtual public void Knockback(Vector3 point, Vector3 dir, float force){
    }

    public void ApplyBuff(BuffApplication buffApp, EntityStat applier){

        GameObject newBuff = (GameObject) Instantiate(buffApp.buffPrefab.gameObject, transform);

        BuffBehavior newBuffBehavior = newBuff.GetComponent<BuffBehavior>();

        newBuffBehavior.ActivateBuff(buffApp, applier);

        buffList.Add(newBuffBehavior);
    }

    public void ApplyBuffEffects(){

        ResetBuffMod();

        buffList.RemoveAll(item => item == null);
        foreach(BuffBehavior buff in buffList){
            buff.ApplyEffect(this);
        }

    }

    public void ResetBuffMod(){
        entityBuffMod.movementMod = 1f;
    }

    public void CalculateResistanceValue(){

        entityResistance.calculatedValue.NormalDamage = entityResistance.FlatResistance.NormalDamage + Global.GetResistanceByArmor(entityResistance.ArmorValue.NormalDamage);
        entityResistance.calculatedValue.ColdDamage = entityResistance.FlatResistance.ColdDamage + Global.GetResistanceByArmor(entityResistance.ArmorValue.ColdDamage);
        entityResistance.calculatedValue.HeatDamage = entityResistance.FlatResistance.HeatDamage + Global.GetResistanceByArmor(entityResistance.ArmorValue.HeatDamage);
        entityResistance.calculatedValue.AcidDamage = entityResistance.FlatResistance.AcidDamage + Global.GetResistanceByArmor(entityResistance.ArmorValue.AcidDamage);
        entityResistance.calculatedValue.VoltDamage = entityResistance.FlatResistance.VoltDamage + Global.GetResistanceByArmor(entityResistance.ArmorValue.VoltDamage);

    }

    public float GetHealthRatio()
    {
    	return currentHealth / entityAttribute.maxHealth;
    }

    public float[] GetResistanceArray()
    {

        float [] result = {
        	entityResistance.calculatedValue.NormalDamage,
        	0f,
        	entityResistance.calculatedValue.ColdDamage,
        	entityResistance.calculatedValue.HeatDamage,
        	entityResistance.calculatedValue.AcidDamage,
        	entityResistance.calculatedValue.VoltDamage
        };

        return result;
    }

    public float GetPierceResistance()
    {
    	return entityResistance.PierceResistance;
    }

    public virtual void Death ()
    {
        entityAttribute.isDead = true;
        gameObject.layer = LayerMask.NameToLayer("Ragdoll");
        gameObject.tag = "Ragdoll";
        Global.Instance.RagdollManager.AddRagdoll(this);
    }
}
