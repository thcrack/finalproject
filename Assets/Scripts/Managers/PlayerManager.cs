﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerManager : MonoBehaviour {

	public PlayerBehavior[] Players;
	public PlayerBehavior MainPlayer;

	int shootableMask, floorMask, interactiveMask;

	public PlayerBehavior selectedPlayer;

	[HideInInspector] public List<PlayerBehavior> AlivePlayers;

	void Awake(){
		AlivePlayers = new List<PlayerBehavior>();
		MainPlayer = Players[0];
		for(int i = 0; i < Players.Length; i++){
			AlivePlayers.Add(Players[i]);
		}

		shootableMask = LayerMask.GetMask("Shootable");
		floorMask = LayerMask.GetMask("Floor");
		interactiveMask = LayerMask.GetMask("Interactive");
	}

	void Update(){

		if(Input.GetButtonDown("Command") && selectedPlayer != null){

			Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast (camRay, out hit, 100f, interactiveMask)) {

				selectedPlayer.playerAIState.currentState.ToInteractState();
				selectedPlayer.interactiveTarget = hit.collider.GetComponent<InteractiveScript>();

			}else if(Physics.Raycast (camRay, out hit, 100f, shootableMask) && hit.collider.GetComponent<EntityStat>().entityAttribute.faction != EntityFaction.Player){

				selectedPlayer.playerAIState.currentState.ToChaseState();
				selectedPlayer.enemyTarget = hit.collider.GetComponent<EntityStat>();

			}else if(Physics.Raycast (camRay, out hit, 100f, floorMask)){

				selectedPlayer.playerAIState.currentState.ToMoveState();
				selectedPlayer.movingTarget = hit.point;

			}
		}

		if(Input.GetButtonDown("All Follow")){
			foreach(PlayerBehavior plr in Players){
				if(!plr.entityAttribute.isDead) plr.playerAIState.currentState.ToFollowState();
			}
		}

		if(Input.GetButtonDown("Select 1")){
			if(!Players[1].entityAttribute.isDead) selectedPlayer = Players[1];
		}else if(Input.GetButtonDown("Select 2")){
			if(!Players[2].entityAttribute.isDead) selectedPlayer = Players[2];
		}else if(Input.GetButtonDown("Select 3")){
			if(!Players[3].entityAttribute.isDead) selectedPlayer = Players[3];
		}
	}

	public void RemoveFromAliveList(PlayerBehavior player){
		AlivePlayers.Remove(player);
	}

	public PlayerBehavior GetRandomAlivePlayer(){
		PlayerBehavior result = null;

		if(AlivePlayers.Count > 0){
			result = AlivePlayers[Random.Range(0, AlivePlayers.Count)];
		}

		return result;
	}

	public int GetAlivePlayerCount(){
		return AlivePlayers.Count;
	}

	public void DisableConcealment(){
		for(int i = 0; i < Players.Length; i++){
			Players[i].CancelConceal();
		}
	}

	public void ActivateAllPlayers(){
		for(int i = 0; i < Players.Length; i++){
			Players[i].gameObject.SetActive(true);
		}
	}

	public bool CheckIsAllDead(){
		return AlivePlayers.Count == 0;
	}
}
