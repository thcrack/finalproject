﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryManager : MonoBehaviour {

	public UIItemListScript UITarget;
	public List<WeaponScript> ItemList;
	public WeaponScript itemHold;

	public int sortType = 0;

	void Awake(){
		ItemList = new List<WeaponScript>();
		foreach(Transform obj in transform){
			ItemList.Add(obj.GetComponent<WeaponScript>());
			obj.gameObject.SetActive(false);
		}
		UITarget.SetItemList(ItemList);
	}

	void Update(){
		if(itemHold != null && Input.GetButtonDown("Fire2")){
			itemHold = null;
		}
	}

	public void AddWeapon(GameObject weaponObject){

		if(ItemList.Contains(weaponObject.GetComponent<WeaponScript>())) return;
		
		weaponObject.GetComponent<WeaponScript>().clipBullets = 0;
		weaponObject.transform.SetParent(transform);
		weaponObject.SetActive(false);
		ItemList.Add(weaponObject.GetComponent<WeaponScript>());
		UITarget.AddNewItem(weaponObject);
	}

	public void RemoveWeaponFromList(GameObject weaponObject){
		ItemList.Remove(weaponObject.GetComponent<WeaponScript>());
	}

	public GameObject GetHeldItem(){
		GameObject weaponObj = itemHold.GetComponent<UIWeaponItem>().targetItem;
		RemoveWeaponFromList(weaponObj);
		Destroy(itemHold);
		return weaponObj;
	}

	public void EquipHeldItemToPlayer(PlayerBehavior player){

		AddWeapon(player.equippedWeapon.gameObject);
		WeaponScript oldWeap = player.equippedWeapon;
		player.equippedWeapon.SetEquipped(false, null);
		player.EquipWeapon(itemHold);
		ItemList.Remove(itemHold);
		UITarget.SetItemList(ItemList);
		itemHold = oldWeap;
		Global.Instance.HUDManager.UpdateInventoryPreview(true);
		Global.Instance.HUDManager.playerPreviewCard.UpdateItemCard(player.equippedWeapon);

	}

	public void SortItemList(){

		switch(sortType){
			case 0: // rarity -> base name -> full name

			ItemList.Sort((x,y) => {
			    var ret = -x.baseWeaponProperty.weaponRarity.CompareTo(y.baseWeaponProperty.weaponRarity);
			    if (ret == 0) ret = x.baseWeaponProperty.weaponName.CompareTo(y.baseWeaponProperty.weaponName);
			    if (ret == 0) ret = x.moddedWeaponProperty.weaponName.CompareTo(y.moddedWeaponProperty.weaponName);
			    return ret;
			});

			break;

			case 1: // base name -> rarity -> full name

			ItemList.Sort((x,y) => {
			    var ret = x.baseWeaponProperty.weaponName.CompareTo(y.baseWeaponProperty.weaponName);
			    if (ret == 0) ret = -x.baseWeaponProperty.weaponRarity.CompareTo(y.baseWeaponProperty.weaponRarity);
			    if (ret == 0) ret = x.moddedWeaponProperty.weaponName.CompareTo(y.moddedWeaponProperty.weaponName);
			    return ret;
			});

			break;

			case 2: // full name -> rarity -> base name

			ItemList.Sort((x,y) => {
			    var ret = x.moddedWeaponProperty.weaponName.CompareTo(y.moddedWeaponProperty.weaponName);
			    if (ret == 0) ret = -x.baseWeaponProperty.weaponRarity.CompareTo(y.baseWeaponProperty.weaponRarity);
			    if (ret == 0) ret = x.baseWeaponProperty.weaponName.CompareTo(y.baseWeaponProperty.weaponName);
			    return ret;
			});

			break;
		}

		sortType++;
		sortType %= 3;

		UITarget.SetItemList(ItemList);
	}
}
