﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RagdollManager : MonoBehaviour {

	List<EntityStat> Ragdolls;

	void Start(){
		Ragdolls = new List<EntityStat>();
	}

	public void AddRagdoll(EntityStat entity){

		if(entity.entityAttribute.faction == EntityFaction.Player) return;

		Ragdolls.Add(entity);
		if(Ragdolls.Count > Global.Instance.gameSettings.maxRagdollCount){
			EntityStat oldestEntity = Ragdolls[0];
			Ragdolls.Remove(oldestEntity);
			Destroy(oldestEntity.gameObject);
		}
	}
}
