﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProgressBarManager : MonoBehaviour {

	public GameObject SliderPrefab;
	public Camera camera;
	public static float ProgressBarOffset = -0.5f;

	public InteractiveSlider CreateNewProgressBar(Vector3 pos){
		Vector3 screenPos = camera.WorldToScreenPoint (pos);
		GameObject newSliderObject = (GameObject) Instantiate (SliderPrefab, gameObject.transform);
		newSliderObject.GetComponent<RectTransform>().localPosition = screenPos;

		InteractiveSlider newSlider = newSliderObject.GetComponent<InteractiveSlider> ();
		newSlider.setOffsetY(ProgressBarOffset);
		newSlider.camera = camera;
		return newSlider;
	}
}
