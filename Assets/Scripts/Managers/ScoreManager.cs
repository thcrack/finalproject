﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;
	int lastScore;


    Text text;


    void Awake ()
    {
        text = GetComponent <Text> ();
        score = 0;
    }


    void Update ()
    {
        text.text = "Score: " + score;
		if (score != lastScore) {
			text.fontSize = 70;
			lastScore = score;
		}
		text.fontSize = Mathf.CeilToInt(Mathf.Lerp(text.fontSize,50,0.5f));
		if (text.fontSize != 50 && text.fontSize < 52)
			text.fontSize = 50;
    }
}
