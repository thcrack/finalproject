﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public bool pauseState = false;

	public GameObject PausePanel;
	public float PauseTimeScale = 0.2f;

	float originalFixedDeltaTime;

	void Awake(){
		PausePanel.SetActive(false);
		originalFixedDeltaTime = Time.fixedDeltaTime;
	}

	void Update(){
		if(Input.GetButtonDown("Pause")){
			SetPause();
		}
	}

	public void SetPause(){
		pauseState = !pauseState;

		if(pauseState){
			Time.timeScale = PauseTimeScale;
			PausePanel.SetActive(true);
			Global.Instance.HUDManager.InitializeHUD();
		}else{
			Time.timeScale = 1f;
			PausePanel.SetActive(false);
			Global.Instance.InventoryManager.itemHold = null;
			Global.Instance.HUDManager.ResetHUD();
		}

		Time.fixedDeltaTime = originalFixedDeltaTime * Time.timeScale;
	}

	public void SetPause(float time){
		pauseState = !pauseState;

		if(pauseState){
			Time.timeScale = PauseTimeScale;
			PausePanel.SetActive(true);
		}else{
			Time.timeScale = 1f;
			PausePanel.SetActive(false);
		}

		Time.fixedDeltaTime = originalFixedDeltaTime * Time.timeScale;

		Invoke("SetPause", time);
	}
}
