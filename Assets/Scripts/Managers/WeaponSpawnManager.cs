﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*

Affix Rarity System

Tier C		1 Rarity
Tier B		2 Rarity
Tier A		3 Rarity
Tier S		4 Rarity

Color		Rarity Name		Minimum Rarity		Affix Combinations
------------------------------------------------------------------

White		Common			0 Total Rarity		No Affix			Tier C
Yellow		Uncommon		2 Total Rarity		Tier B				Tier C + Tier C
Green		Rare			3 Total Rarity		Tier B + Tier C		Tier B + Tier B
Blue		Fabled			Unique Item Set #1	No Affix
Magenta		Unreal			4 Total Rarity		Tier B + Tier C		Tier B + Tier B
Purple		Mythical		6 Total Rarity		Tier A + Tier A		Tier S + Tier B		1 Tier S + Tier A
Orange		Legendary		Unique Item	Set #2	No Affix
Cyan		Omniscient		8 Total Rarity		Tier S + Tier S


*/

public enum WeaponRarity {
	Common 		= 0,
	Uncommon 	= 1,
	Rare 		= 2,
	Fabled		= 3,
	Unreal		= 4,
	Mythical	= 5,
	Legendary	= 6,
	Omniscient	= 7
}



[System.SerializableAttribute]
public struct RarityWeight {
	public int CommonWeight;
	public int UncommonWeight;
	public int RareWeight;
	public int FabledWeight;
	public int UnrealWeight;
	public int MythicalWeight;
	public int LegendaryWeight;
	public int OmniscientWeight;

	public int[] GetArray(){
		return new int[]{
			CommonWeight,
			UncommonWeight,
			RareWeight,
			FabledWeight,
			UnrealWeight,
			MythicalWeight,
			LegendaryWeight,
			OmniscientWeight
		};
	}

	public static RarityWeight operator +(RarityWeight a, RarityWeight b){

		RarityWeight result;

		result.CommonWeight = a.CommonWeight + b.CommonWeight;
		result.UncommonWeight = a.UncommonWeight + b.UncommonWeight;
		result.RareWeight = a.RareWeight + b.RareWeight;
		result.FabledWeight = a.FabledWeight + b.FabledWeight;
		result.UnrealWeight = a.UnrealWeight + b.UnrealWeight;
		result.MythicalWeight = a.MythicalWeight + b.MythicalWeight;
		result.LegendaryWeight = a.LegendaryWeight + b.LegendaryWeight;
		result.OmniscientWeight = a.OmniscientWeight + b.OmniscientWeight;

		return result;
	}
}

public class WeaponSpawnManager : MonoBehaviour {

	[System.SerializableAttribute]
	public class ItemDropInfo{
		public int baseDropAmount;
		public RarityWeight weightMod;
		public float[] AdditionalDropChance;
	}

	[Header("Base Rarity Drop Weight")]

	public RarityWeight rarityWeight;

	[Header("Enemy Drop Property")]

	public ItemDropInfo CommonEnemyDrop;
	public ItemDropInfo SpecialEnemyDrop;
	public ItemDropInfo LegendEnemyDrop;
	public ItemDropInfo BossEnemyDrop;
	public ItemDropInfo PodEnemyDrop;

	[Header("Basic Weapon List")]

	public List<GameObject> baseWeaponList;
	public List<GameObject> UniqueWeaponList1;
	public List<GameObject> UniqueWeaponList2;

	[Header("Weapon Modifier List")]

	public List<WeaponModifier> modPrefixList;
	public List<WeaponModifier> modSuffixList;
	public WeaponModifier modEmpty;

	List<WeaponModifier>[] modPrefixRarityList;
	List<WeaponModifier>[] modSuffixRarityList;

	void Awake(){
		InitializeAffixRarityLists();
	}

	public GameObject SpawnWeapon(Vector3 pos, Vector3 forceDir, RarityWeight weightMod){
		pos.y += 2f;
		GameObject baseWeapon;
		WeaponModifier modPrefix;
		WeaponModifier modSuffix;

		WeaponRarity rarityRoll = GetDropRarity(weightMod);

		SelectBasedOnRarity(rarityRoll, out baseWeapon, out modPrefix, out modSuffix);

		GameObject newWeapon = (GameObject) Instantiate(baseWeapon, pos, Random.rotation);

		Rigidbody rigidbody = newWeapon.GetComponent<Rigidbody>();
		rigidbody.isKinematic = false;

		if(forceDir != Vector3.zero){

			rigidbody.AddForce(Vector3.up * 3f + forceDir.normalized * 6f, ForceMode.Impulse);

		}else{

			rigidbody.AddForce(Vector3.up * 3f + Vector3.right * Random.Range(-3f, 3f) + Vector3.forward * Random.Range(-3f, 3f), ForceMode.Impulse);
		
		}
		

		Collider collider = newWeapon.GetComponent<Collider>();
		collider.enabled = true;

		WeaponScript newWeaponScript = newWeapon.GetComponent<WeaponScript>();
		newWeaponScript.baseWeaponProperty.weaponRarity = rarityRoll;
		newWeaponScript.AssignAffix(modPrefix, modSuffix);
		newWeaponScript.LoadClip();

		return newWeapon;	
	}

	public GameObject SpawnWeapon(GameObject original){

		GameObject newWeapon = (GameObject) Instantiate(original, original.transform.position, original.transform.rotation);

		Rigidbody rigidbody = newWeapon.GetComponent<Rigidbody>();
		rigidbody.isKinematic = false;

		BoxCollider boxCollider = newWeapon.GetComponent<BoxCollider>();
		boxCollider.enabled = true;

		return newWeapon;

	}

	public void SpawnMultipleWeapon(Vector3 pos, Vector3 forceDir, int weaponCount, RarityWeight weightMod){

		for(int i = 0; i < weaponCount; i++){
			SpawnWeapon(pos, forceDir, weightMod);
		}

	}

	public void RollWeaponOnDeath(Vector3 pos, EnemyType enemyType){

		float[] chance = {0f};
		RarityWeight weightMod = new RarityWeight();
		int spawnCount = 0;

		switch(enemyType){
			case EnemyType.Common:

			spawnCount += CommonEnemyDrop.baseDropAmount;
			chance = CommonEnemyDrop.AdditionalDropChance;
			weightMod = CommonEnemyDrop.weightMod;

			break;

			case EnemyType.Special:

			spawnCount += SpecialEnemyDrop.baseDropAmount;
			chance = SpecialEnemyDrop.AdditionalDropChance;
			weightMod = SpecialEnemyDrop.weightMod;

			break;

			case EnemyType.Legend:

			spawnCount += LegendEnemyDrop.baseDropAmount;
			chance = LegendEnemyDrop.AdditionalDropChance;
			weightMod = LegendEnemyDrop.weightMod;

			break;

			case EnemyType.Boss:

			spawnCount += BossEnemyDrop.baseDropAmount;
			chance = BossEnemyDrop.AdditionalDropChance;
			weightMod = BossEnemyDrop.weightMod;

			break;

			case EnemyType.Pod:

			spawnCount += PodEnemyDrop.baseDropAmount;
			chance = PodEnemyDrop.AdditionalDropChance;
			weightMod = PodEnemyDrop.weightMod;

			break;
		}

		for(int i = 0; i < chance.Length; i++){

			if(Random.value <= chance[i]){
				spawnCount ++;
			}else{
				break;
			}

		}

		SpawnMultipleWeapon(pos, Vector3.zero, spawnCount, weightMod);

	}

	void InitializeAffixRarityLists(){
		modPrefixRarityList = new List<WeaponModifier>[4];
		for(int i = 0; i < modPrefixRarityList.Length; i++){
			modPrefixRarityList[i] = new List<WeaponModifier>();
		}
		foreach(WeaponModifier pre in modPrefixList){
			switch(pre.rarity){
				case AffixRarity.TierC:
				modPrefixRarityList[0].Add(pre);
				break;

				case AffixRarity.TierB:
				modPrefixRarityList[1].Add(pre);
				break;

				case AffixRarity.TierA:
				modPrefixRarityList[2].Add(pre);
				break;

				case AffixRarity.TierS:
				modPrefixRarityList[3].Add(pre);
				break;
			}
		}

		modSuffixRarityList = new List<WeaponModifier>[4];
		for(int i = 0; i < modSuffixRarityList.Length; i++){
			modSuffixRarityList[i] = new List<WeaponModifier>();
		}
		foreach(WeaponModifier suf in modSuffixList){
			switch(suf.rarity){
				case AffixRarity.TierC:
				modSuffixRarityList[0].Add(suf);
				break;

				case AffixRarity.TierB:
				modSuffixRarityList[1].Add(suf);
				break;

				case AffixRarity.TierA:
				modSuffixRarityList[2].Add(suf);
				break;

				case AffixRarity.TierS:
				modSuffixRarityList[3].Add(suf);
				break;
			}
		}
	}

	WeaponRarity GetDropRarity(WeaponRarity minRarity){

		int dropRarity = 0;
		int[] dropWeights = rarityWeight.GetArray();

		int totalWeight = 0;
		for(int i = (int)minRarity; i < dropWeights.Length; i++){
			totalWeight += dropWeights[i];
		}

		int lot = Random.Range(0, totalWeight);
		for(int i = (int)minRarity; i < dropWeights.Length; i++){

			if(lot < dropWeights[i]){
				dropRarity = i;
				break;
			}

			lot -= dropWeights[i];
		}

		return (WeaponRarity)dropRarity;
	}

	WeaponRarity GetDropRarity(RarityWeight weightMod){

		int dropRarity = 0;
		RarityWeight resultWeight = rarityWeight + weightMod;
		int[] dropWeights = resultWeight.GetArray();

		int totalWeight = 0;
		for(int i = 0; i < dropWeights.Length; i++){
			if(dropWeights[i] > 0)	totalWeight += dropWeights[i];
		}

		int lot = Random.Range(0, totalWeight);
		for(int i = 0; i < dropWeights.Length; i++){

			if(lot < dropWeights[i]){
				dropRarity = i;
				break;
			}

			if(dropWeights[i] > 0)	lot -= dropWeights[i];
		}

		return (WeaponRarity)dropRarity;
	}

	void SelectBasedOnRarity(WeaponRarity rarity, out GameObject selectedWeapon, out WeaponModifier selectedPrefix, out WeaponModifier selectedSuffix){

		// get weapon from list
		switch(rarity){
			case WeaponRarity.Fabled:
			selectedWeapon = UniqueWeaponList1[Random.Range(0,UniqueWeaponList1.Count)];
			break;

			case WeaponRarity.Legendary:
			selectedWeapon = UniqueWeaponList2[Random.Range(0,UniqueWeaponList2.Count)];
			break;

			default:
			selectedWeapon = baseWeaponList[Random.Range(0,baseWeaponList.Count)];
			break;
		}
		
		selectedPrefix = modEmpty;
		selectedSuffix = modEmpty;

		// select rarity -> select combination -> select pre/suf order -> pick from pre/suf lists

		int rarityNumber = (int)rarity;

		switch(rarityNumber){
			// white
			case 0:
			switch(Random.Range(0,2)){
				// no affix
				case 0:
				selectedPrefix = modEmpty;
				selectedSuffix = modEmpty;
				break;

				// Tier C + Empty
				case 1:
				if(Random.Range(0,2) == 0){
					selectedPrefix = modPrefixRarityList[0][Random.Range(0,modPrefixRarityList[0].Count)];
					selectedSuffix = modEmpty;
				}else{
					selectedPrefix = modEmpty;
					selectedSuffix = modSuffixRarityList[0][Random.Range(0,modSuffixRarityList[0].Count)];
				}
				break;
			}
			break;

			// yellow
			case 1:
			switch(Random.Range(0,2)){
				// B + empty
				case 0:
				if(Random.Range(0,2) == 0){
					selectedPrefix = modPrefixRarityList[1][Random.Range(0,modPrefixRarityList[1].Count)];
					selectedSuffix = modEmpty;
				}else{
					selectedPrefix = modEmpty;
					selectedSuffix = modSuffixRarityList[1][Random.Range(0,modSuffixRarityList[1].Count)];
				}
				break;

				// Tier C + Empty
				case 1:
				selectedPrefix = modPrefixRarityList[0][Random.Range(0,modPrefixRarityList[0].Count)];
				selectedSuffix = modSuffixRarityList[0][Random.Range(0,modSuffixRarityList[0].Count)];
				break;
			}
			break;

			// green
			case 2:
			switch(Random.Range(0,2)){
				// B + C
				case 0:
				if(Random.Range(0,2) == 0){
					selectedPrefix = modPrefixRarityList[1][Random.Range(0,modPrefixRarityList[1].Count)];
					selectedSuffix = modSuffixRarityList[0][Random.Range(0,modSuffixRarityList[0].Count)];
				}else{
					selectedPrefix = modPrefixRarityList[0][Random.Range(0,modPrefixRarityList[0].Count)];
					selectedSuffix = modSuffixRarityList[1][Random.Range(0,modSuffixRarityList[1].Count)];
				}
				break;

				// B + B
				case 1:
				selectedPrefix = modPrefixRarityList[1][Random.Range(0,modPrefixRarityList[1].Count)];
				selectedSuffix = modSuffixRarityList[1][Random.Range(0,modSuffixRarityList[1].Count)];
				break;
			}
			break;

			// blue
			case 3:
			break;

			// magenta
			case 4:
			switch(Random.Range(0,2)){
				// A + C
				case 0:
				if(Random.Range(0,2) == 0){
					selectedPrefix = modPrefixRarityList[2][Random.Range(0,modPrefixRarityList[2].Count)];
					selectedSuffix = modSuffixRarityList[0][Random.Range(0,modSuffixRarityList[0].Count)];
				}else{
					selectedPrefix = modPrefixRarityList[0][Random.Range(0,modPrefixRarityList[0].Count)];
					selectedSuffix = modSuffixRarityList[2][Random.Range(0,modSuffixRarityList[2].Count)];
				}
				break;

				// A + B
				case 1:
				if(Random.Range(0,2) == 0){
					selectedPrefix = modPrefixRarityList[2][Random.Range(0,modPrefixRarityList[2].Count)];
					selectedSuffix = modSuffixRarityList[1][Random.Range(0,modSuffixRarityList[1].Count)];
				}else{
					selectedPrefix = modPrefixRarityList[1][Random.Range(0,modPrefixRarityList[1].Count)];
					selectedSuffix = modSuffixRarityList[2][Random.Range(0,modSuffixRarityList[2].Count)];
				}
				break;
			}
			break;

			// purple
			case 5:
			switch(Random.Range(0,3)){
				// A + A
				case 0:
				selectedPrefix = modPrefixRarityList[2][Random.Range(0,modPrefixRarityList[2].Count)];
				selectedSuffix = modSuffixRarityList[2][Random.Range(0,modSuffixRarityList[2].Count)];
				break;

				// S + B
				case 1:
				if(Random.Range(0,2) == 0){
					selectedPrefix = modPrefixRarityList[3][Random.Range(0,modPrefixRarityList[3].Count)];
					selectedSuffix = modSuffixRarityList[1][Random.Range(0,modSuffixRarityList[1].Count)];
				}else{
					selectedPrefix = modPrefixRarityList[1][Random.Range(0,modPrefixRarityList[1].Count)];
					selectedSuffix = modSuffixRarityList[3][Random.Range(0,modSuffixRarityList[3].Count)];
				}
				break;

				// S + A
				case 2:
				if(Random.Range(0,2) == 0){
					selectedPrefix = modPrefixRarityList[3][Random.Range(0,modPrefixRarityList[3].Count)];
					selectedSuffix = modSuffixRarityList[2][Random.Range(0,modSuffixRarityList[2].Count)];
				}else{
					selectedPrefix = modPrefixRarityList[2][Random.Range(0,modPrefixRarityList[2].Count)];
					selectedSuffix = modSuffixRarityList[3][Random.Range(0,modSuffixRarityList[3].Count)];
				}
				break;
			}
			break;

			// orange
			case 6:
			break;

			// cyan
			case 7:
			selectedPrefix = modPrefixRarityList[3][Random.Range(0,modPrefixRarityList[3].Count)];
			selectedSuffix = modSuffixRarityList[3][Random.Range(0,modSuffixRarityList[3].Count)];
			break;
		}
	}

}
