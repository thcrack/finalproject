﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDManager : MonoBehaviour {

	public Transform DamageTextParent;
	public GameObject DamageTextPrefab;

	public Transform ItemCardParent;
	public GameObject ItemCardPrefab;

	public ItemCard defaultItemCard;

	public float DamageTextOffset = 1f;

	[Header("Pause Screen: Inventory Panel")]
	public ItemCard inventoryPreviewCard;
	public ItemCard playerPreviewCard;
	public GameObject playerEquipSection;
	public UIPlayerEquippingButton[] playerEquippingButtons;

	void Start(){
		for(int i = 0; i < playerEquippingButtons.Length; i++){
			playerEquippingButtons[i].assignedPlayer = Global.Instance.PlayerManager.Players[i];
		}
		ResetHUD();
	}

	public void CreateDamagePopup(Transform target, float dmgValue, Color color, bool isCrit){
		Vector3 pos = target.position;
		Vector3 randVec = new Vector3(Random.Range(-DamageTextOffset, DamageTextOffset), 0.5f, Random.Range(-DamageTextOffset, DamageTextOffset));
		pos += randVec;
		Vector3 screenPos = Camera.main.WorldToScreenPoint (pos);
		GameObject newDamageText = (GameObject) Instantiate (DamageTextPrefab, DamageTextParent);
		newDamageText.transform.localPosition = screenPos;
		newDamageText.transform.localRotation = Quaternion.identity;
		newDamageText.transform.localScale = Vector3.one;

		UIDamageText newTextScript = newDamageText.GetComponent<UIDamageText>();
		newTextScript.followTarget = target.position;
		newTextScript.offset = randVec;
		if(color == DamageInfo.GetDamageColor(DamageType.RAD)){
			newTextScript.isRAD = true;
		}

		Text nText = newTextScript.text;

		nText.text = Global.ShortenNumberToText(dmgValue);
		nText.color = color;
		nText.fontSize = Mathf.FloorToInt(30f * Global.Instance.gameSettings.damageNumberScale) + Random.Range(-3, 3);

		if(isCrit) newTextScript.EnableCritEffect();
	}

	public ItemCard CreateItemCard(WeaponScript targetItem){

		GameObject newCard = (GameObject) Instantiate(ItemCardPrefab, ItemCardParent);
		newCard.transform.localRotation = Quaternion.identity;
		newCard.transform.localScale = Vector3.one;
		ItemCard newCardScript = newCard.GetComponent<ItemCard>();
		newCardScript.UpdateItemCard(targetItem);
		newCard.SetActive(false);
		return newCardScript;

	}

	public void UpdateInventoryPreview(bool setActive){
		if(!setActive){
			inventoryPreviewCard.gameObject.SetActive(false);
		}else{
			inventoryPreviewCard.UpdateItemCard(Global.Instance.InventoryManager.itemHold);
			inventoryPreviewCard.gameObject.SetActive(true);
			playerEquipSection.SetActive(true);
		}
	}

	public void ResetHUD(){
		UpdateInventoryPreview(false);
		playerEquipSection.SetActive(false);
		playerPreviewCard.gameObject.SetActive(false);
		Global.Instance.InventoryManager.sortType = 0;
	}

	public void InitializeHUD(){
		for(int i = 0; i < playerEquippingButtons.Length; i++){
			if(playerEquippingButtons[i].assignedPlayer.entityAttribute.isDead) playerEquippingButtons[i].interactable = false;
		}
		Vector3 ancP = Global.Instance.InventoryManager.UITarget.GetComponent<RectTransform>().anchoredPosition;
		ancP.y = 0;
		Global.Instance.InventoryManager.UITarget.GetComponent<RectTransform>().anchoredPosition = ancP;
		
	}

	public void CreateDamagePopup(HitInfo hitInfo){
		
	}
}
