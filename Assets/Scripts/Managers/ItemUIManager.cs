﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemUIManager : MonoBehaviour {

	public GameObject TextObject;
	Text text;

	void Awake(){
		text = TextObject.GetComponent<Text>();
	}

	public void SetItemMessage(WeaponScript weap, Vector3 screenPos){
		screenPos.y += 40f;
		TextObject.transform.position = screenPos;
		text.enabled = true;
		text.text = weap.GetStylizedName();
	}

	public void DisableItemMessage(){
		text.enabled = false;
	}
}
