﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IDScript : MonoBehaviour {

	Text IDtext;
	float currentValue = 0f;
	float colorDelta = 0.3f;

	// Use this for initialization
	void Start () {
		IDtext = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		IDtext.color = Color.HSVToRGB(currentValue,1,1);
		currentValue += colorDelta * Time.deltaTime;
		currentValue %= 1f;
	}
}
