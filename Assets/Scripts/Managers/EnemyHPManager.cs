﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyHPManager : MonoBehaviour {

	public GameObject SliderPrefab;
	public GameObject DamageTextPrefab;
	public float DamageTextOffset = 1f;
	public Camera camera;
	public static float HPBarOffset = 1.5f;

	public InteractiveSlider createNewUI(Vector3 pos, EnemyType enemyType){
		Vector3 screenPos = camera.WorldToScreenPoint (pos);
		GameObject newSliderObject = (GameObject) Instantiate (SliderPrefab, screenPos, Quaternion.identity);
		newSliderObject.transform.SetParent(gameObject.transform);

		float sliderWidth = 60f;
		float sliderHeight = 10f;

		switch(enemyType){
			case EnemyType.Common:
			HPBarOffset = 1.5f;
			sliderWidth = 60f;
			break;

			case EnemyType.Special:
			HPBarOffset = 1.5f;
			sliderWidth = 80f;
			break;

			case EnemyType.Legend:
			HPBarOffset = 2f;
			sliderWidth = 160f;
			break;

			case EnemyType.Boss:
			HPBarOffset = 4f;
			sliderWidth = 250f;
			sliderHeight = 30f;
			break;
		}

		InteractiveSlider newSlider = newSliderObject.GetComponent<InteractiveSlider> ();
		newSlider.setSize(sliderWidth, sliderHeight);
		newSlider.setOffsetY(HPBarOffset);
		newSlider.camera = camera;
		return newSlider;
	}

	public void createDamageNumber(Vector3 pos, float dmgValue){
		int dmgValueInt = Mathf.RoundToInt(dmgValue);
		Vector3 randVec = new Vector3(Random.Range(-DamageTextOffset, DamageTextOffset),Random.Range(-DamageTextOffset, DamageTextOffset),Random.Range(-DamageTextOffset, DamageTextOffset));
		pos += randVec;
		Vector3 screenPos = camera.WorldToScreenPoint (pos);
		GameObject newDamageText = (GameObject) Instantiate (DamageTextPrefab, screenPos, Quaternion.identity);
		newDamageText.transform.SetParent(transform);
		Text nText = newDamageText.GetComponent<Text>();
		nText.text = dmgValueInt.ToString();
		nText.fontSize = 28 + Random.Range(-5, 5);
	}
}
