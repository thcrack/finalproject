﻿using UnityEngine;
using System.Collections;

public enum DamageNumberMode{
	None,
	Single,
	Multiple
}

public class Global : Singleton<Global> {

	[System.SerializableAttribute]
	public class GameMechanics{
		public float TKDamagePercentage = 0.01f;
		public float baseCritMultiplier = 1.2f;
		[Range(0f,1f)]
		public float baseCritChance = 0.15f;
		public float ArmorToEHPPercentage = 0.0025f;
		public float LevelPower = 1.08f;
		public float LevelPowerAdditionPerLevel = 0.01f;
		public float DOTTickInterval = 0.5f;
		public float MinimumEntityMovementMod = 0.05f;
	}

	[System.SerializableAttribute]
	public class GameSettings{
		public int maxRagdollCount = 10;
		public float projectileAutoDestructTime = 10f;
		public DamageNumberMode damageNumberMode = DamageNumberMode.Multiple;
		[Range(0.5f, 2f)]
		public float damageNumberScale = 1f;
	}

	public bool CinemaMode = false;

	public WeaponSpawnManager WeaponSpawnManager;
	public BuffDatabase BuffDatabase;
	public PlayerManager PlayerManager;
	public InventoryManager InventoryManager;
	public GameManager GameManager;
	public RagdollManager RagdollManager;
	public HUDManager HUDManager;
	public Transform EnemyManager;
	public WeaponModifier EmptyAffix;

	public GameMechanics gameMechanics;
	public GameSettings gameSettings;

	void Start(){
		Debug.Log(GetResistanceByArmor(9999f));
	}

	public static float GetResistanceByArmor(float armor){
		return armor * Global.Instance.gameMechanics.ArmorToEHPPercentage / (1 + Mathf.Abs(armor) * Global.Instance.gameMechanics.ArmorToEHPPercentage);
	}

	public static float GetLevelPower(int level){

		return Mathf.Pow(Global.Instance.gameMechanics.LevelPower, level) * ( 1 + level * Global.Instance.gameMechanics.LevelPowerAdditionPerLevel );
	}

	public static string ColorToHex(Color32 color)
	{
		string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
		return hex;
	}
	 
	public static Color HexToColor(string hex)
	{
		byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		return new Color32(r,g,b, 255);
	}

	public static string ShortenNumberToText(float input){
		string result = "";
		if(input < 1000){
			result = Mathf.CeilToInt(input).ToString();
		}else if(input < 1000000){
			result =  Mathf.Round(input/100)/10 + "K";
		}else{
			result =  Mathf.Round(input/100000)/10 + "M";
		}
		return result;
	}

}
