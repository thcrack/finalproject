using UnityEngine;
using System.Collections;

public class ProjectileBehavior : MonoBehaviour {


	public bool isLaunched = false;
	public bool isCrit = false;
	public bool hasCollided = false;
	public float projectileSpeed;
	public float explosionRange = 5f;
	public WeaponScript sourceWeapon;
	public float collisionFXLifetime = 3f;
	public float collisionLightIntensity = 5f;
	float fxTimer;
	float endTime;

	public Vector3 direction;

	Rigidbody rigidbody;
	Light light;

	void Awake(){
		rigidbody = GetComponent<Rigidbody>();
		light = GetComponent<Light>();
	}

	void FixedUpdate(){
		if(hasCollided){
			fxTimer -= Time.fixedDeltaTime;
			light.intensity = Mathf.Lerp(light.intensity, 0f, 12f * Time.fixedDeltaTime);
			if(fxTimer <= 0f) KillSelf();
		}else if(isLaunched){
			rigidbody.velocity = direction * projectileSpeed;
			if(Time.time >= endTime) Destroy(gameObject);
		}
	}

	public void SetSpeed(float speed){

		projectileSpeed = speed;

	}

	public void SetDirection(Vector3 dir){

		direction = dir.normalized;

	}

	public void SetColor(Color color){
		light.color = color;
		Renderer[] rends = GetComponentsInChildren<Renderer>();
		GetComponent<ParticleSystem>().startColor = color;
		GetComponent<TrailRenderer>().material.SetColor("_TintColor", color);

        foreach(Renderer rend in rends){

            Material mat = rend.material;
            mat.SetColor("_EmissionColor", color);

        }
	}

	public void Launch(){
		isLaunched = true;
		endTime = Time.time + Global.Instance.gameSettings.projectileAutoDestructTime;
		explosionRange = sourceWeapon.moddedWeaponProperty.weaponPiercePower;
	}

	void OnCollisionEnter(Collision other){
		Rigidbody otherRgd = other.collider.GetComponent<Rigidbody>();
		Vector3 otherRelation = other.collider.transform.position - transform.position;
		if(otherRgd != null) otherRgd.AddForceAtPosition(otherRelation.normalized * sourceWeapon.moddedWeaponProperty.weaponKnockbackPower, other.contacts[0].point, ForceMode.Impulse);
		transform.GetChild(0).gameObject.SetActive(false);
		GetComponent<Collider>().enabled = false;
		rigidbody.isKinematic = true;
		hasCollided = true;
		light.range = explosionRange * 3f;
		light.intensity = collisionLightIntensity;
		fxTimer = collisionFXLifetime;
		GetComponent<ParticleSystem>().startSize = explosionRange * 2.5f;
		GetComponent<ParticleSystem>().Play();

		Collider[] hits = Physics.OverlapSphere(transform.position, explosionRange, Physics.DefaultRaycastLayers, QueryTriggerInteraction.Ignore);
		foreach(Collider col in hits){

			if(col.gameObject.layer == gameObject.layer) continue;

			EntityStat entityStat = col.GetComponent<EntityStat>();
			Rigidbody rgd = col.GetComponent<Rigidbody>();

			Vector3 offsetPos = new Vector3(col.transform.position.x, transform.position.y, col.transform.position.z);

			Vector3 relation = offsetPos - transform.position;

			float decayRatio = Mathf.Min(1f, (1f - (Vector3.Distance(transform.position, offsetPos) / explosionRange)) + 0.5f);
			bool isObstructed = Physics.Linecast(transform.position, offsetPos, LayerMask.GetMask("Wall"));

			if(!isObstructed){
				if(	entityStat != null){

					DamageInfo decayedDamage = sourceWeapon.moddedWeaponProperty.weaponDamage * decayRatio;
					HitInfo hitInfo = new HitInfo(decayedDamage, sourceWeapon.weaponHolder, entityStat, isCrit, sourceWeapon.moddedWeaponProperty.weaponCritMultiplier);
					entityStat.Hit(hitInfo, true);
					entityStat.Knockback(offsetPos, relation, sourceWeapon.moddedWeaponProperty.weaponKnockbackPower * decayRatio);
				}else if(rgd != null){
					rgd.AddExplosionForce(sourceWeapon.moddedWeaponProperty.weaponKnockbackPower, transform.position, explosionRange, 1.2f, ForceMode.Impulse);
	                //rgd.AddForceAtPosition(relation.normalized * sourceWeapon.moddedWeaponProperty.weaponKnockbackPower, transform.position, ForceMode.Impulse);
	            }
	        }
		}
	}

	void KillSelf(){
		Destroy(gameObject);
	}
	
}