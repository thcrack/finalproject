﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIDirectionIndicator : MonoBehaviour {

	public bool isEnabled = true;
	public Transform target;
	public bool isPlayer;
	public float offsetY = 3f;
	public float shrinkRatio = 0.6f;
	public float shrinkAlpha = 0.1f;
	public Color selectedColor;

	Image image;
	Color originalColor;

	void Start(){
		Debug.Log(Screen.width + " / " + Screen.height);
		image = GetComponent<Image>();
		image.enabled = isEnabled;
		transform.GetChild(0).gameObject.SetActive(isEnabled);
		originalColor = image.color;
	}
	
	void Update(){

		if(Camera.main != null){

			Vector3 offsetPos = target.position;
			Vector3 targetScale = new Vector3(shrinkRatio, shrinkRatio, shrinkRatio);
			Quaternion targetQuat = Quaternion.identity;
			float targetAlpha = shrinkAlpha;
			offsetPos.y += offsetY;

			Vector3 screenPos = Camera.main.WorldToScreenPoint(offsetPos);
			screenPos.z = 0f;

			if (screenPos.x < 0
			|| screenPos.x > Screen.width
			|| screenPos.y < 0
			|| screenPos.y > Screen.height){

				Vector3 screenMidPoint = new Vector3(Screen.width/2, Screen.height/2, 0f);
				screenPos.x = Mathf.Clamp(screenPos.x, 0f, Screen.width);
				screenPos.y = Mathf.Clamp(screenPos.y, 0f, Screen.height);
				Vector3 relation = screenPos - screenMidPoint;
				float angle = Vector3.Angle(relation, Vector3.down);
				if(relation.x < 0) angle = -angle;
				targetQuat = Quaternion.Euler(0f, 0f, angle);
				targetScale = Vector3.one;
				targetAlpha = 1f;

			}

			transform.localPosition = screenPos;
			transform.localRotation = Quaternion.RotateTowards(transform.localRotation, targetQuat, 540f * Time.deltaTime);
			transform.localScale = Vector3.Lerp(transform.localScale, targetScale, 10f * Time.deltaTime);

			if(isPlayer && target.GetComponent<PlayerBehavior>() == Global.Instance.PlayerManager.selectedPlayer){
				image.color = selectedColor;
			}else{
				image.color = originalColor;
			}

			Color newColor = image.color;
			newColor.a = Mathf.Lerp(newColor.a, targetAlpha,  10f * Time.deltaTime);
			image.color = newColor;

		}

	}

	public void Trigger(){
		isEnabled = !isEnabled;
		image.enabled = isEnabled;
		transform.GetChild(0).gameObject.SetActive(isEnabled);
	}
}
