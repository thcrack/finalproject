﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum PlayerIndicatorType{
	InteractionAndReload,
	Health
}

public class UIPlayerInteractiveIndicator : MonoBehaviour {

	public bool isWorldSpace;
	public PlayerIndicatorType type = PlayerIndicatorType.InteractionAndReload;

	public Image backgroundImage;
	public Image fillImage;

	public PlayerBehavior assignedPlayer;
	public float OffsetY = 0.05f;

	void Start(){
		ToggleGraphics(false);
	}

	void Update(){

		if(assignedPlayer.entityAttribute.isDead){
			ToggleGraphics(false);
		}else{

			float state = -1f;

			switch(type){
				case PlayerIndicatorType.InteractionAndReload:
				state = assignedPlayer.GetInteractionProgress();
				break;

				case PlayerIndicatorType.Health:
				state = assignedPlayer.GetHealthRatio();
				break;
			}

			if(state >= 0){

				ToggleGraphics(true);
				fillImage.fillAmount = state;

				if(Camera.main != null){

					if(isWorldSpace){

						Vector3 newPos = assignedPlayer.transform.position;
						newPos.y += OffsetY;
						transform.position = newPos;

					}else{

						Vector3 screenPos = Camera.main.WorldToScreenPoint(assignedPlayer.transform.position);
						transform.localPosition = screenPos;

					}

				}

			}else{

				ToggleGraphics(false);

			}
		}
	}

	void ToggleGraphics(bool input){
		backgroundImage.enabled = input;
		fillImage.enabled = input;
	}
}
