﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemCard : MonoBehaviour {

	public bool worldSpace = true;
	public bool autoUpdatePosition = true;
	public WeaponScript assignedWeapon;

	public Text titleText;
	public Text damageNumber;
	public Text rateNumber;
	public Text pierceText;
	public Text pierceNumber;
	public Text accuracyNumber;
	public Text knockbackNumber;
	public Text reloadTimeNumber;
	public Text reloadTypeNumber;
	public Text clipSizeNumber;

	public Text descriptionText;
	public Text descWeaponTypeText;
	public Transform damageBar;

	void FixedUpdate(){

		if(autoUpdatePosition) UpdatePosition(Input.mousePosition);
		
	}

	public void UpdateItemCard(WeaponScript weapon){
		assignedWeapon = weapon;
		UpdateItemCard();
	}

	public void UpdateItemCard(){
		if(assignedWeapon != null){

			// set title
			titleText.text = "<color=" + assignedWeapon.GetRarityColorName() + ">" + assignedWeapon.moddedWeaponProperty.weaponName + "</color>";

			// set card outline color as the largest damage type color
			//GetComponent<Image>().color = DamageInfo.GetDamageColor(assignedWeapon.moddedWeaponProperty.weaponDamage.GetLargestDamageType(true));
			GetComponent<Image>().color = assignedWeapon.GetRarityColor();

			// set damage number
			float dmgSum = assignedWeapon.moddedWeaponProperty.weaponDamage.Sum();
			damageNumber.text = dmgSum.ToString();

			// set pellet multiplier
			if(assignedWeapon.moddedWeaponProperty.weaponPelletsCount > 1){
				damageNumber.text += " <color=orange><size=" + (damageNumber.fontSize - 3) + ">X" + assignedWeapon.moddedWeaponProperty.weaponPelletsCount + "</size></color>";
			}

			// set rpm number
			rateNumber.text = assignedWeapon.moddedWeaponProperty.weaponRPM.ToString();

			// set pierce text and number
			if(assignedWeapon.moddedWeaponProperty.weaponType == WeaponType.ProjectileLauncher){
				pierceText.text = "Radius";
			}else{
				pierceText.text = "Pierce";
			}
			pierceNumber.text = assignedWeapon.moddedWeaponProperty.weaponPiercePower.ToString();

			// set accuracy text
			accuracyNumber.text = assignedWeapon.moddedWeaponProperty.weaponAccuracy.ToString();

			// set knockback text
			knockbackNumber.text = assignedWeapon.moddedWeaponProperty.weaponKnockbackPower.ToString();

			// set reload time text
			reloadTimeNumber.text = assignedWeapon.moddedWeaponProperty.weaponReloadTime.ToString();

			// set reload type text
			reloadTypeNumber.text = assignedWeapon.moddedWeaponProperty.weaponReloadType.ToString();

			// set clip size text
			clipSizeNumber.text = assignedWeapon.moddedWeaponProperty.weaponClipSize.ToString();

			//set description text
			descWeaponTypeText.text = "";
			descWeaponTypeText.text += assignedWeapon.moddedWeaponProperty.weaponRarity.ToString() + " " + assignedWeapon.GetTypeString();
			descWeaponTypeText.color = assignedWeapon.GetRarityColor();
			descriptionText.text = "";
			foreach(BuffApplication buffApp in assignedWeapon.moddedWeaponProperty.buffApplicationList){
				descriptionText.text += "★ " + buffApp.GetDescription() + "\n";
			}

			// set damage bar
			float[] dmgDistArray = assignedWeapon.moddedWeaponProperty.weaponDamage.GetArray();
			float parentWidth = damageBar.GetComponent<RectTransform>().sizeDelta.x;
			for(int i = 0; i < 6; i++){
				damageBar.GetChild(i).GetComponent<LayoutElement>().preferredWidth = (dmgDistArray[i] / dmgSum) * parentWidth;
			}
		}
	}

	public void UpdatePosition(Vector3 pos){
		if(worldSpace){
			transform.localPosition = pos;
			transform.localRotation = Quaternion.identity;
		}else{
			transform.position = pos;
		}

		RectTransform rect = GetComponent<RectTransform>();
		Vector2 newPivot = new Vector2(0f, 0f);

		if(pos.x > Screen.width - rect.sizeDelta.x){
			newPivot.x = 1f;
		}

		if(pos.y > Screen.height - rect.sizeDelta.y){
			newPivot.y = 1f;
		}

		rect.pivot = newPivot;
	}

}
