﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIDamageText : MonoBehaviour {

	public Text text;
	public Image textBackground;

	public float DecayTime = 0.5f;
	public float FadeCurve = 2f;
	public float RisingDistance = 6f;
	public Font critFont;
	public int critAdditionalSize = 10;
	public float critDecayTime = 0.8f;
	public float critFadeCurve = 4f;
	public Font radFont;
	public bool isRAD = false;
	float RADvalue = 0f;
	float RADdelta = 3f;

	Color textColor;
	float startTime;
	[HideInInspector] public Vector3 offset;
	[HideInInspector] public Vector3 followTarget;

	void Awake(){
		startTime = Time.time;
		textBackground.enabled = false;
	}

	public void EnableCritEffect(){

		textBackground.enabled = true;
		text.font = critFont;
		text.fontSize = text.fontSize + critAdditionalSize;
		text.GetComponent<Shadow>().enabled = true;
		FadeCurve = critFadeCurve;
		DecayTime = critDecayTime;

	}

	void Update(){
		if(isRAD) text.font = radFont;
		if(Time.time < startTime + DecayTime){

			float passProgress = (Time.time - startTime) / DecayTime;
			if(isRAD){

				textColor = Color.HSVToRGB(RADvalue,1,1);
				RADvalue += RADdelta * Time.deltaTime;
				RADvalue %= 1f;

			}else{

				textColor = text.color;

			}

			textColor.a = (1f - passProgress) * FadeCurve;
			if(textBackground.enabled){
				Color bgColor = textBackground.color;
				bgColor.a = textColor.a / 2f;
				textBackground.color = bgColor;
			}
			text.color = textColor;

			Vector3 risingOffset = new Vector3(0f, RisingDistance * passProgress, 0f);

			Vector3 screenPos = Camera.main.WorldToScreenPoint(followTarget + offset + risingOffset);

			transform.localPosition = screenPos;

		}else{

			Destroy(gameObject);

		}
	}
}
