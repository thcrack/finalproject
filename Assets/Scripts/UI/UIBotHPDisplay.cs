﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIBotHPDisplay : MonoBehaviour {

	public Text text;

	public PlayerBehavior PlayerReference;

	void Update(){
		if(PlayerReference.currentHealth > 0){
			text.text = "HP: " + PlayerReference.currentHealth + " / " + PlayerReference.entityAttribute.maxHealth;
		}else{
			text.text = "<color=red>KILLED IN ACTION</color>";
		}
	}
}
