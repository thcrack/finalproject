﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIItemListScript : MonoBehaviour {

	public GameObject itemUIPrefab;
	public float ItemHeight = 50f;
	public int ItemCount;

	RectTransform rect;

	// Use this for initialization
	void Start () {
	
		rect = GetComponent<RectTransform>();
		ItemCount = rect.childCount;
		rect.sizeDelta = new Vector2(0f, ItemHeight * ItemCount);
		
	}
	
	// Update is called once per frame
	void Update () {

		if(ItemCount != rect.childCount){
			ItemCount = rect.childCount;
			rect.sizeDelta = new Vector2(0f, ItemHeight * ItemCount);
			
		}
	
	}

	public void SetItemList(List<WeaponScript> itemList){

		DestroyAllItems();

		for(int i = 0; i < itemList.Count; i++){
			GameObject newItem = (GameObject) Instantiate(itemUIPrefab, transform);
			newItem.GetComponent<UIWeaponItem>().AssignItem(itemList[i].gameObject);
		}
	}

	public void AddNewItem(GameObject input){
		GameObject newItem = (GameObject) Instantiate(itemUIPrefab, transform);
		newItem.GetComponent<UIWeaponItem>().AssignItem(input);
	}

	void DestroyAllItems(){
		int indexLimit = rect.childCount;
		for(int i = indexLimit; i > 0; i--){
			Destroy(rect.GetChild(i - 1).gameObject);
		}
	}
}
