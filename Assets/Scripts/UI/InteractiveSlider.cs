﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InteractiveSlider : MonoBehaviour {

	public Camera camera;

	float offsetY = 0f;
	Slider slider;

	void Awake(){
		slider = GetComponent<Slider> ();
	}

	public void changeDisplay(float ratio){
		slider.value = ratio;
	}

	public void setOffsetY(float input){
		offsetY = input;
	}

	public void setPosition(Vector3 pos){
		Vector3 offsetPos = pos;
		offsetPos.y += offsetY;
		transform.position = camera.WorldToScreenPoint(offsetPos);
		transform.localScale = Vector3.one;
	}

	public void setSize(float w, float h){
		RectTransform rect = GetComponent<RectTransform> ();
		rect.sizeDelta = new Vector2(w,h);
	}

	public void kill(){
		Destroy (gameObject);
	}
}
