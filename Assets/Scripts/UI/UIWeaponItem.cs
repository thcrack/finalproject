﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIWeaponItem : MonoBehaviour {

	UIItemListScript itemList;
	public GameObject targetItem;
	public Text text;

	void Awake(){
		itemList = transform.parent.GetComponent<UIItemListScript>();
	}

	public void AssignItem(GameObject input){
		targetItem = input;
		text.text = targetItem.GetComponent<WeaponScript>().GetStylizedName();
	}

	public void Hold(){
		Global.Instance.InventoryManager.itemHold = targetItem.GetComponent<WeaponScript>();
		Global.Instance.HUDManager.UpdateInventoryPreview(true);
	}
}
