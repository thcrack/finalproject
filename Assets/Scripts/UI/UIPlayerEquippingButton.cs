﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UIPlayerEquippingButton : Button, IPointerEnterHandler, IPointerExitHandler {

	public PlayerBehavior assignedPlayer;

	public override void OnPointerEnter(PointerEventData eventData){

		base.OnPointerEnter(eventData);

		if(assignedPlayer.entityAttribute.isDead) return;

		Global.Instance.HUDManager.playerPreviewCard.UpdateItemCard(assignedPlayer.equippedWeapon);
		Global.Instance.HUDManager.playerPreviewCard.gameObject.SetActive(true);

	}

	public override void OnPointerExit(PointerEventData eventData){

		base.OnPointerExit(eventData);

		if(assignedPlayer.entityAttribute.isDead) return;

		Global.Instance.HUDManager.playerPreviewCard.gameObject.SetActive(false);

	}

	public void AssignCurrentItemToPlayer(){
		Global.Instance.InventoryManager.EquipHeldItemToPlayer(assignedPlayer);
	}
}
