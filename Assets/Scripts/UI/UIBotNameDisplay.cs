﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIBotNameDisplay : MonoBehaviour {

	public Text text;
	public PlayerBehavior PlayerReference;

	void Awake(){
		if(PlayerReference != null) UpdateName();
	}

	public void UpdateName(){
		text.text = PlayerReference.playerName;
	}
}
