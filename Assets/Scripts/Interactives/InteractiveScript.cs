﻿using UnityEngine;
using System.Collections;
using WellFired;

public enum InteractiveType {
	LootChest, Trigger
}

public enum TriggerMethod {
	Interaction,
	EnteringTrigger,
	AllAlivePlayersEnteringTrigger,
	ToggleOnChange
}

public class InteractiveScript : MonoBehaviour {

	public Transform BotDestination;

	public float InteractionTime = 3f;
	public bool Interactable = true;
	public bool Refreshable = false;
	public float RefreshInterval = 10f;
	public bool disableColliderWhenOffline = true;

	public Material disabledMaterial;
	Material originalMaterial;
	Renderer rend;

	public InteractiveType interactiveType = InteractiveType.Trigger;

	[Header("Trigger Property")]
	public TriggerMethod triggerMethod = TriggerMethod.Interaction;
	public GameObject[] TriggerTargets;
	int inRangePlayerCount = 0;

	[Header("Loot Property")]
	public int minLootDrop = 2;
	public int maxLootDrop = 5;
	public RarityWeight weightMod;

	Light light;
	int defLayer, intLayer;

	void Awake(){

		rend = GetComponent<Renderer>();
		if(rend != null) originalMaterial = rend.material;

		light = GetComponent<Light>();
		defLayer = LayerMask.NameToLayer("Default");
		intLayer = LayerMask.NameToLayer("Interactive");

	}

	void FixedUpdate(){
		if(interactiveType == InteractiveType.Trigger
			&& triggerMethod == TriggerMethod.AllAlivePlayersEnteringTrigger
			&& inRangePlayerCount == Global.Instance.PlayerManager.GetAlivePlayerCount())
		{
				Interact(Global.Instance.PlayerManager.GetRandomAlivePlayer().gameObject);
		}
	}

	void OnTriggerEnter(Collider col){

		if(triggerMethod != TriggerMethod.ToggleOnChange && col.gameObject.tag != "Player" || !Interactable) return;

		inRangePlayerCount ++;
		if(interactiveType == InteractiveType.Trigger)
		{
			switch(triggerMethod){
				case TriggerMethod.EnteringTrigger:
				Interact(col.gameObject);
				break;

				case TriggerMethod.AllAlivePlayersEnteringTrigger:
				if(inRangePlayerCount == Global.Instance.PlayerManager.GetAlivePlayerCount()) Interact(col.gameObject);
				break;

				case TriggerMethod.ToggleOnChange:
				if(inRangePlayerCount == 1) Interact(col.gameObject);
				break;

				case TriggerMethod.Interaction:
				break;
			}
		}
	}

	void OnTriggerExit(Collider col){

		if(triggerMethod != TriggerMethod.ToggleOnChange && col.gameObject.tag != "Player") return;

		inRangePlayerCount --;
		if(triggerMethod == TriggerMethod.ToggleOnChange && inRangePlayerCount == 0) Interact(col.gameObject);
	}

	public void Interact(GameObject target){
		if(!Interactable) return;
		Debug.Log("INTERACTED BY " + target.name);
		switch(interactiveType){
			case InteractiveType.LootChest:

				Vector3 relation = target.transform.position - transform.position;
				WeaponSpawnManager wpnMngr = Global.Instance.WeaponSpawnManager;
				wpnMngr.SpawnMultipleWeapon(transform.position, relation, Random.Range(minLootDrop, maxLootDrop), weightMod);

			break;

			case InteractiveType.Trigger:

				for(int i = 0; i < TriggerTargets.Length; i++){
					if(TriggerTargets[i] != null){
						if(TriggerTargets[i].GetComponent<USSequencer>() != null){
							TriggerTargets[i].GetComponent<USSequencer>().Play();
						}else{
							TriggerTargets[i].SendMessage("Trigger");
						}
					}
				}

			break;
		}

		Interactable = false;
		gameObject.layer = defLayer;
		if(disableColliderWhenOffline) GetComponent<Collider>().enabled = false;
		if(disabledMaterial != null) rend.material = disabledMaterial;


		if(light != null) light.enabled = false;

		if(Refreshable){
			Invoke("Refresh", RefreshInterval);
		}
	}

	void Refresh(){
		Interactable = true;
		gameObject.layer = intLayer;
		GetComponent<Collider>().enabled = true;
		if(rend != null) rend.material = originalMaterial;
		if(light != null) light.enabled = true;
	}
}
