public interface IDamageable{
	
	void DamageHit(DamageInfo dmgInfo);

}