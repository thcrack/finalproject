public interface IPlayerAIState{
	
	// States:
	// - Follow the main player
	// - Move towards a point
	// - Move towards an interative
	// - Move towards an enemy until able to attack it

	void UpdateState();
	void FixedUpdateState();

	void ToFollowState();
	void ToMoveState();
	void ToInteractState();
	void ToChaseState();

}