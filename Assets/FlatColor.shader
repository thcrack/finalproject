﻿Shader "unityCookie/tut/beginner/1 - Flat Color" {
	//Interface
	Properties {
		_Color ("Color", Color) = (1.0,1.0,1.0,1.0)
		_SubColor ("Sub Color", Color) = (0.0,0.0,0.0,1.0)
		_Border ("Border", float) = 0.5
	}

	SubShader {
		Pass {
			CGPROGRAM
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			
			//user defined variables
			uniform float4 _Color;
			uniform float4 _SubColor;
			uniform float _Border;
			
			//base input structs
			struct vertexInput
			{
				float4 vertex : POSITION;
			};
			struct vertexOutput
			{
				float4 pos : SV_POSITION;
			};
			
			//vertex function
			vertexOutput vert(vertexInput v)
			{
				vertexOutput o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}
			
			//fragment function
			float4 frag(vertexOutput i) : COLOR
			{
				if(i.pos.x > _Border){
					return _SubColor;
				}else{
					return _Color;
				}
			}
			
			ENDCG
		}
	}
	//fallback commented out during development
	//Fallback "Diffuse"
}